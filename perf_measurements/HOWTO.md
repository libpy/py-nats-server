# Measuring perf howtos:

# NATS

```sh
curl -L https://github.com/nats-io/nats-server/releases/download/v2.10.7/nats-server-v2.10.7-linux-amd64.zip -o nats-server.zip

unzip nats-server.zip -d nats-server

cp nats-server/nats-server-v2.10.7-linux-amd64/nats-server ./nats-server-v2.10.7

rm nats-server.zip
rm -r nats-server
```

# PY-NATS

Using [pyinstaller](https://pyinstaller.org/en/stable/index.html) to generate executable from the python code to be just started on docker.

This creates /py_nats folder in which there will be executable py_nats:
```sh
pyinstaller --distpath ./ ../src/sockets.py --name=py_nats

./py_nats/py_nats
```

# Docker bash for stress testing:

Execute from project dir:
```sh
sudo docker run --name ubuntu-dock --cpuset-cpus="0" -p 4222:4222 --rm -it -v $(pwd)/perf_measurements:/usr/py-nats/ ubuntu
```

This creates Ubuntu based Docker, can use only 1 core, port forwarding of 4222 and mount of current to /usr/src/py-nats-server

This will be used for performance testing, to level the playing with Python single core only for starters...

# NATS-CLI (Stress testing tool)


```sh
curl -L  https://github.com/nats-io/natscli/releases/download/v0.1.1/nats-0.1.1-linux-amd64.zip -o nats-cli.zip

unzip nats-cli.zip -d nats-cli

cp nats-cli/nats-0.1.1-linux-amd64/nats ./cli-nats-v0.1.1

rm nats-cli.zip
rm -r nats-cli
```

Create context (and select it) for 127.0.0.1:4222 
```sh
./cli-nats-v0.1.1 context add local --description "Localhost" --select

```

After which commands can be executed like:
rtt = round trip time
```sh
./cli-nats-v0.1.1 rtt 

./cli-nats-v0.1.1 bench foo --pub 1 --sub 1 --size 16 --msgs 1000000
```