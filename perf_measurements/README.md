# Perf improvments

Simple 1:1 pubsub on 1M msgs 16b msg size:

Init version [23d078c5](https://gitlab.com/libpy/py-nats-server/-/commit/23d078c503733e6192de6dadecbc0487ee0041cf), 
most naive and simplest implementation
```sh
./cli-nats-v0.1.1 bench foo --pub 1 --sub 1 --size 16 --msgs 1000000
15:19:00 Starting Core NATS pub/sub benchmark [subject=foo, multisubject=false, multisubjectmax=100000, msgs=1,000,000, msgsize=16 B, pubs=1, subs=1, pubsleep=0s, subsleep=0s]
15:19:00 Starting subscriber, expecting 1,000,000 messages
15:19:00 Starting publisher, publishing 1,000,000 messages
Finished      9s [=======================================================================================================================] 100%
Finished      6s [=======================================================================================================================] 100%

NATS Pub/Sub stats: 205,296 msgs/sec ~ 3.13 MB/sec
 Pub stats: 102,648 msgs/sec ~ 1.57 MB/sec
 Sub stats: 102,650 msgs/sec ~ 1.57 MB/sec
```

Now I need some profiler to see, I have my candidates for optimizing but usually human intuition is misleading - so don't guess but measure!
Lets ask a piece of software (a profiler) to tell us were were is the code spending most of it's time :)

[pyinstrument](https://github.com/joerick/pyinstrument) to the rescue!

I wrapped "handle_new_socket" function into profile start -> stop (on disconnect)
And results were very interesting:
```sh
15.443 Handle._run  asyncio/events.py:78
└─ 15.424 handle_new_socket  sockets.py:7
   ├─ 11.422 new_cmd  nats_cmds.py:116
   │  ├─ 8.138 publish_msg  nats_cmds.py:65
   │  │  ├─ 4.413 StreamWriter.write  asyncio/streams.py:324
   │  │  │     [4 frames hidden]  asyncio, <built-in>
   │  │  │        3.862 socket.send  <built-in>
   │  │  ├─ 1.656 [self]  nats_cmds.py
   │  │  ├─ 0.894 debug  logging/__init__.py:2140
   │  │  │     [4 frames hidden]  logging
   │  │  └─ 0.827 StreamWriter.drain  asyncio/streams.py:348
   │  │        [2 frames hidden]  asyncio
   │  ├─ 1.429 [self]  nats_cmds.py
   │  ├─ 0.949 debug  logging/__init__.py:2140
   │  │     [4 frames hidden]  logging
   │  ├─ 0.497 pub_try_parse  msg_parser.py:31
   │  │  └─ 0.302 [self]  msg_parser.py
   │  └─ 0.164 connect_try_parse  msg_parser.py:4
   ├─ 2.138 [self]  sockets.py
   └─ 1.756 StreamReader.readuntil  asyncio/streams.py:536
         [5 frames hidden]  asyncio, <built-in>
```
**The [self] in output refers to the time taken by function excluding other functions called**

Very interesting:
1) Most of the time was spend inside publish_msg function, and on StreamWriter.write
2) Interesting to me, log.debug is showing... yea makes sense as methods are being called,
and parameter strings are being calculated so they impact performance...

After removing all log.debug:

```sh
NATS Pub/Sub stats: 273,999 msgs/sec ~ 4.18 MB/sec
 Pub stats: 136,999 msgs/sec ~ 2.09 MB/sec
 Sub stats: 137,003 msgs/sec ~ 2.09 MB/sec
```
It's a very nice jump, from 3.13 MB/sec to 4.18 MB/sec => ~33% jump!

And what about trying to buffer 1 msg, just 1 nothing more:
(instead of sending the received message right away, wait for another and then send together)

```sh
NATS Pub/Sub stats: 384,061 msgs/sec ~ 5.86 MB/sec
 Pub stats: 192,030 msgs/sec ~ 2.93 MB/sec
 Sub stats: 192,038 msgs/sec ~ 2.93 MB/sec
```
A respectable 45% jump!

what about 4:
```sh
NATS Pub/Sub stats: 506,768 msgs/sec ~ 7.73 MB/sec
 Pub stats: 253,384 msgs/sec ~ 3.87 MB/sec
 Sub stats: 253,399 msgs/sec ~ 3.87 MB/sec
```
buffering 10 msgs:
```sh
NATS Pub/Sub stats: 606,780 msgs/sec ~ 9.26 MB/sec
 Pub stats: 303,390 msgs/sec ~ 4.63 MB/sec
 Sub stats: 303,407 msgs/sec ~ 4.63 MB/sec
```

for 20 msgs was just a little bit better, and going further it was not better.

Most likely, OS it self has some limit were it's starting to fragment packages to much and we are getting no more increase after that amount of bytes. Or there is a simpler explanation that then - it's just that some other piece of code is slowing things down to much that further improvements there yield overall no improvements.

[asyncio/streams.py#L22](https://github.com/python/cpython/blob/4cf7bb8e22bf37e6d65bf4cb5618d09c4a8ad612/Lib/asyncio/streams.py#L22)
```py
_DEFAULT_LIMIT = 2 ** 16  # 64 KiB
```

**Meaning it could be up to 64 KiB, above that and the performance will go down!**
P.S. From the future: This is just a limit for StreamReaders. And even in StreamReader cases depends on which method you are using (.read ignores this limit)

Lets see now how perf looks without log.debug and with 10 msg buffered:

```sh
7.754 Handle._run  asyncio/events.py:78
└─ 7.744 handle_new_socket  sockets.py:10
   ├─ 3.931 new_cmd  nats_cmds.py:131
   │  ├─ 1.647 publish_msg  nats_cmds.py:68
   │  │  ├─ 0.710 [self]  nats_cmds.py
   │  │  ├─ 0.628 StreamWriter.write  asyncio/streams.py:324
   │  │  │     [3 frames hidden]  asyncio, <built-in>
   │  │  └─ 0.084 StreamWriter.drain  asyncio/streams.py:348
   │  ├─ 1.344 [self]  nats_cmds.py
   │  ├─ 0.491 pub_try_parse  msg_parser.py:31
   │  │  └─ 0.290 [self]  msg_parser.py
   │  ├─ 0.179 connect_try_parse  msg_parser.py:4
   │  │  └─ 0.102 [self]  msg_parser.py
   │  ├─ 0.138 ping_try_parse  msg_parser.py:13
   │  └─ 0.104 PublishInfo.__init__  nats_cmds.py:27
   ├─ 2.102 [self]  sockets.py
   ├─ 1.627 StreamReader.readuntil  asyncio/streams.py:536
   │     [6 frames hidden]  asyncio, <built-in>
   └─ 0.084 bytes.decode  <built-in>
```
Ok now things look more balanced and it's time to start thinking of improvements like:
1) Looking at raw bytes instead of string.
2) Making a better keys for dictionary lookup, or removing a need to create some big string on the fly
...

After commenting log.debug and implementing rudimentary msg count buffer for sending msgs (100 at the time) [fe17e270](https://gitlab.com/libpy/py-nats-server/-/commit/fe17e2700a22b6e200fda5daa766570f3c4ff4c1)

```sh
NATS Pub/Sub stats: 705,030 msgs/sec ~ 10.76 MB/sec
 Pub stats: 352,657 msgs/sec ~ 5.38 MB/sec
 Sub stats: 352,633 msgs/sec ~ 5.38 MB/sec
```

After doing large refactoring [5850e9dd](https://gitlab.com/libpy/py-nats-server/-/commit/5850e9ddf43c7041504ea4ae8201fad1f6e37d7b), replacing str with bytes and putting in place a real BufferedWriter (perf didn't change much):

```sh
NATS Pub/Sub stats: 730,733 msgs/sec ~ 11.15 MB/sec
 Pub stats: 365,510 msgs/sec ~ 5.58 MB/sec
 Sub stats: 365,385 msgs/sec ~ 5.58 MB/sec
```

```sh
6.297 _UnixSelectorEventLoop._run_once  asyncio/base_events.py:1832
└─ 6.296 Handle._run  asyncio/events.py:78
   └─ 6.279 handle_new_socket  server.py:15
      ├─ 1.711 [self]  server.py
      ├─ 1.572 StreamReader.readuntil  asyncio/streams.py:536
      │     [6 frames hidden]  asyncio, <built-in>
      ├─ 1.499 Messenger.publish_msg  messenger.py:105
      │  ├─ 0.783 [self]  messenger.py
      │  └─ 0.585 BufferedWriter.write  client.py:47
      │     └─ 0.424 [self]  client.py
      └─ 1.497 SupportedCmds.new_msg  commands.py:227
         ├─ 0.683 CmdWrap.try_prepare  commands.py:60
         │  ├─ 0.349 [self]  commands.py
         │  └─ 0.334 PubCmd.try_split_to_parts  commands.py:95
         │     └─ 0.217 [self]  commands.py
         ├─ 0.593 CmdWrap.execute  commands.py:74
         │  ├─ 0.481 PubCmd.execute  commands.py:107
         │  │  ├─ 0.365 Messenger.announce_pub  messenger.py:99
         │  │  │  ├─ 0.199 [self]  messenger.py
         │  │  │  └─ 0.166 PublishInfo.__init__  client.py:13
         │  │  └─ 0.116 [self]  commands.py
         │  └─ 0.112 [self]  commands.py
         └─ 0.221 [self]  commands.py
``` 

Looks like it's spending some meaningful time in StreamReader.readuntil so new idea is to try and do the same as in case of writing, instead of doing reader.readuntil (new line), read more bytes at bulk and then process them by splitting.

Experimenting with reading 8KiB at the time yielded better results:
```sh
NATS Pub/Sub stats: 983,623 msgs/sec ~ 15.01 MB/sec
 Pub stats: 492,071 msgs/sec ~ 7.51 MB/sec
 Sub stats: 491,855 msgs/sec ~ 7.51 MB/sec
``` 

and 

```sh

3.934 Handle._run  asyncio/events.py:78
└─ 3.913 handle_new_socket  server.py:17
   ├─ 1.579 SupportedCmds.new_msg  commands.py:227
   │  ├─ 0.728 CmdWrap.try_prepare  commands.py:60
   │  │  ├─ 0.365 [self]  commands.py
   │  │  └─ 0.363 PubCmd.try_split_to_parts  commands.py:95
   │  │     ├─ 0.236 [self]  commands.py
   │  │     └─ 0.065 bytes.split  <built-in>
   │  ├─ 0.596 CmdWrap.execute  commands.py:74
   │  │  ├─ 0.489 PubCmd.execute  commands.py:107
   │  │  │  ├─ 0.350 Messenger.announce_pub  messenger.py:99
   │  │  │  │  ├─ 0.178 PublishInfo.__init__  client.py:13
   │  │  │  │  └─ 0.172 [self]  messenger.py
   │  │  │  └─ 0.139 [self]  commands.py
   │  │  └─ 0.107 [self]  commands.py
   │  └─ 0.255 [self]  commands.py
   ├─ 1.461 Messenger.publish_msg  messenger.py:105
   │  ├─ 0.768 [self]  messenger.py
   │  ├─ 0.575 BufferedWriter.write  client.py:47
   │  │  ├─ 0.447 [self]  client.py
   │  │  └─ 0.046 perf_counter  <built-in>
   │  └─ 0.052 bytes.split  <built-in>
   └─ 0.834 [self]  server.py
``` 
Change for above is made in [ea71d76e](https://gitlab.com/libpy/py-nats-server/-/commit/ea71d76ef9a1d73809615a659399498847fec387)

This fight of performance optimization come quite far. PY-NATS started with at **~ 3.13 MB/sec** and now it's at **15.01 MB/sec** almost 5 folds. Not bad.

Still it's far from around **130 MB/sec** a real nats can do on my machine... This means there are still improvements to be made.

I want to do a little bit of simplifying and deleting code before I continue further with optimizations:
1) Remove the "smart buffer" which cleans it self out when no more msg arrive after some timeout. There is really no need for this, no need for such complexity as it can be replaced by writing everything when one hole batch of messages (up to 256KiB of data) is processed.

2) The whole # subject -> clientId1:sid1->consumer1,... Dict to Dict in Messenger feels like to complex. I will simplify it.

3) Usage of None is polluting server code for nothing really. I will just return b"" when no response and always do .write


All of the above is implemented in [3d1140e](https://gitlab.com/libpy/py-nats-server/-/commit/3d1140eb25beb46d9447f8e52ce0f4ec8febeaa5)

With removal of "Smart buffer" a respectable 5MB (~30%) perf increase (this was also due to doing b"".join(c.buffer) instead of +=)

```sh
NATS Pub/Sub stats: 1,329,738 msgs/sec ~ 20.29 MB/sec
 Pub stats: 665,596 msgs/sec ~ 10.16 MB/sec
 Sub stats: 666,008 msgs/sec ~ 10.16 MB/sec
```

And in profiler no more "0.575 BufferedWriter.write" and from taking 3.934 seconds it went down to 2.794 seconds 

```sh
2.794 _UnixSelectorEventLoop._run_once  asyncio/base_events.py:1832
└─ 2.793 Handle._run  asyncio/events.py:78
   └─ 2.785 handle_new_socket  server.py:21
      ├─ 1.526 SupportedCmds.new_msg  commands.py:227
      │  ├─ 0.707 CmdWrap.try_prepare  commands.py:60
      │  │  ├─ 0.372 PubCmd.try_split_to_parts  commands.py:95
      │  │  │  ├─ 0.255 [self]  commands.py
      │  │  │  ├─ 0.062 bytes.split  <built-in>
      │  │  │  └─ 0.029 len  <built-in>
      │  │  └─ 0.335 [self]  commands.py
      │  ├─ 0.598 CmdWrap.execute  commands.py:74
      │  │  ├─ 0.456 PubCmd.execute  commands.py:107
      │  │  │  ├─ 0.346 Messenger.announce_pub  messenger.py:169
      │  │  │  │  ├─ 0.205 [self]  messenger.py
      │  │  │  │  └─ 0.141 PublishInfo.__init__  client.py:12
      │  │  │  └─ 0.110 [self]  commands.py
      │  │  └─ 0.142 [self]  commands.py
      │  └─ 0.221 [self]  commands.py
      ├─ 0.755 Messenger.publish_msg  messenger.py:175
      │  ├─ 0.613 [self]  messenger.py
      │  ├─ 0.068 list.append  <built-in>
      │  └─ 0.047 any  <built-in>
      ├─ 0.398 [self]  server.py
      └─ 0.054 list.append  <built-in>
```

After reading a bit on Python and doing a lot of experimentation (and going in wrong direction many times...) here are the important bits for future improvements:

1) **Bytes** type is immutable. It's really fast to .split and .find however you can't change the values inside, and with each function call copy is created.
2) **Memoryview** type is mutable and provides a **zero-copy** interface for reading and writing slices of objects.
3) **Bytearray** type provides a mutable bytes-like type that can be used for zero-copy data reads with functions like socket.recv_from.
4) A memoryview can wrap a bytearray, allowing for received data to be spliced into an arbitrary buffer location without copying costs.

**+= for bytes (and string) really kills of the performance** As bytes are immutable with each call it created new memory and again new memory which is slow.
Do b"".join(LIST of bytes) 

Now lets attack a couple of things which should provide more performance improvements:
1) Move from += to b"".join 
2) "0.141 PublishInfo.__init__": When doing messages in batch (256 KiB at the time) no need to do PUB in two parts (first just take pub info and then do the pub). If message i is PUB then message i+1 is the payload, so just take it ad don't create the PublishInfo objects all over the place
3) Add base part of MSG INFO in Subscription itself so it's not created from scratch each time...
4) As bytes are immutable, it's better to do the splitting of cmd parts inside Server and passing list of bytes to cmds then passing the bytes and splitting later.

All of the above is implemented in [9a4d452](https://gitlab.com/libpy/py-nats-server/-/commit/9a4d4524c287001c25d6777017111f75a177036b)

1) -> noop - checked code base, and already fixed in all places 
2) -> gave around +1Mb/s
3) -> gave around +4Mb/s
4) -> speed everything by 2x, giving +25Mb/s. This was done at the cost of code readability it's more complex, more edge cases and stuff but it's worth it.

Now testing with 10M msgs as 1M is to fast, getting inconsistent measurements.
```sh
NATS Pub/Sub stats: 3,437,492 msgs/sec ~ 52.45 MB/sec
 Pub stats: 1,718,961 msgs/sec ~ 26.23 MB/sec
 Sub stats: 1,718,969 msgs/sec ~ 26.23 MB/sec
```

```sh
11.524 _UnixSelectorEventLoop._run_once  asyncio/base_events.py:1832
└─ 11.519 Handle._run  asyncio/events.py:78
   └─ 11.460 handle_new_socket  server.py:20
      ├─ 4.914 Messenger.publish_msg  messenger.py:171
      │  ├─ 3.920 [self]  messenger.py
      │  ├─ 0.414 any  <built-in>
      │  ├─ 0.297 len  <built-in>
      │  └─ 0.283 list.extend  <built-in>
      ├─ 4.175 [self]  server.py
      ├─ 0.685 bytes.startswith  <built-in>
      ├─ 0.614 bytes.split  <built-in>
      ├─ 0.311 bytes.join  <built-in>
      ├─ 0.310 list.append  <built-in>
      ├─ 0.211 bytes.splitlines  <built-in>
      └─ 0.122 Profiler.output_text  pyinstrument/profiler.py:287
            [5 frames hidden]  pyinstrument
```
Looks well mostly work is happening in publish_msg and also letter when sending the MSG.

I am satisfied finally, it was a long fight :D. Having a half of performance is a Milestone I am proud of!

TBD: https://jiffyclub.github.io/snakeviz/ this looks great! 






