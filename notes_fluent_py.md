
- Since Python 3.7, the dict type is officially “ordered,” but that only
means that the key insertion order is preserved. You cannot
rearrange the keys in a dict however you like.

- In Python code, line breaks are ignored inside pairs of [], {}, or ().

- Listcomps do everything the map and filter functions do, without the contortions of the functionally challenged Python lambda
```py
colors = ['black', 'white'] sizes = ['S', 'M', 'L']
tshirts = [(color, size) for color in colors 
                         for size in sizes]
```

- Generator yield, can be created with listcomp but using () instead of []. Also, a set or dict can be created by using {}

- Tuples Are Not Just Immutable Lists but also Tuples hold records: each item in the tuple holds the data for one field, and the position of the item gives its meaning

```py
>>> def fixed(o):
... try:
...     hash(o)
... except TypeError:
...     return False
... return True
...
>>> tf = (10, 'alpha', (1, 2))
>>> tm = (10, 'alpha', [1, 2])
>>> fixed(tf)
True
>>> fixed(tm)
False
```

- Tuples are heavily used in Python core instead of List. They take less memory, can be clearer (wont be changed fix size) and faster overall:
https://stackoverflow.com/questions/68630/are-tuples-more-efficient-than-lists-in-python/22140115#22140115

- Unpacking is important because it avoids unnecessary and error-prone use of indexes to extract elements from sequences, also this is somewhat cool:

```py
# swap
b, a = a, b

# take rest
a, b, *rest = range(5)

# also this weirdness..
a, *body, c, d = range(5)


>>> def fun(a, b, c, d, *rest):
... return a, b, c, d, rest
...
>>> fun(*[1, 2], 3, *range(4, 7))
(1, 2, 3, 4, (5, 6))

exp = [...]
(_, x) = exp
return x

def handle_command(self, message):
    match message:
        case ['BEEPER', frequency, times]:
            self.beep(times, frequency)
        case ['NECK', angle]:
            self.rotate_neck(angle)
        case ['LED', ident, intensity]:
            self.leds[ident].set_brightness(ident, intensity)
        case ['LED', ident, red, green, blue]:
            self.leds[ident].set_color(ident, red, green, blue)
        case _:
            raise InvalidCommand(message)

case [name, _, _, (lat, lon) as coord]:

case [str(name), _, _, (float(lat), float(lon))]:
must match types

case [str(name), *_, (float(lat), float(lon))]:
The *_ matches any number of items, without binding them to a variable. Using
*extra instead of *_ would bind the items to extra as a list with 0 or more items.

case [name, _, _, (lat, lon)] if lon <= 0:
the last item must be a two-item sequence.

```
- https://github.com/gvanrossum/patma/blob/3ece6444ef70122876fd9f0099eb9490a2d630df/EXAMPLES.md#case-6-a-very-deep-iterable-and-type-match-with-extraction

```py
def evaluate(exp: Expression, env: Environment) -> Any:
"Evaluate an expression in an environment."
if isinstance(x, Symbol):          # variable reference
        return env[x]
    elif not isinstance(x, List):  # constant literal
        return x
    elif x[0] == 'quote':          # (quote exp)
        (_, exp) = x
        return exp
    elif x[0] == 'if':             # (if test conseq alt)
        (_, test, conseq, alt) = x
        exp = (conseq if eval(test, env) else alt)
        return eval(exp, env)
    elif x[0] == 'define':         # (define var exp)
        (_, var, exp) = x
        env[var] = eval(exp, env)
    elif x[0] == 'lambda':         # (lambda (var...) body)
        (_, parms, body) = x
        return Procedure(parms, body, env)
# ... more lines omitted
```

```py
def evaluate(exp: Expression, env: Environment) -> Any:
    "Evaluate an expression in an environment."
    match exp:
    # ... lines omitted
        case ['quote', x]:
            return x
        case ['if', test, consequence, alternative]:
            if evaluate(test, env):
                return evaluate(consequence, env)
            else:
                return evaluate(alternative, env)
        case ['lambda', [*parms], *body] if body:
            return Procedure(parms, body, env)
        case ['define', Symbol() as name, value_exp]:
            env[name] = evaluate(value_exp, env)
# ... more lines omitted
```

- Pattern matching is an example of declarative programming: the code describes “what” you want to match, instead of “how” to match it

- The best defense of the Python convention of excluding the last item in ranges and slices was written by Edsger W. Dijkstra himself, in a short memo titled [“Why Numbering Should Start at Zero”](https://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD831.html). The subject of the memo is mathematical notation, but it’s relevant to Python because Dijkstra explains with rigor and humor why a sequence like 2, 3, ..., 12 should always be expressed as 2 ≤ i < 13. All other reasonable conventions are refuted, as is the idea of letting each user choose a convention.
The title refers to zero-based indexing, but the memo is really about why it is desirable that 'ABCDE'[1:3] means 'BC' and not 'BCD' and why it makes perfect sense to write range(2, 13) to produce 2, 3, 4, ..., 12. By the way, the memo is a handwritten note, but it’s beautiful and totally readable. Dijkstra’s handwriting is so clear that someone created a [font](https://www.fonts101.com/fonts/view/Uncategorized/34398/Dijkstra) out of his notes.

- **SLICES**

```py
>>> s = 'bicycle'
>>> s[::3]
'bye'
>>> s[::-1]
'elcycib'
>>> s[::-2]
'eccb'

```

```py
>>> weird_board = [['_'] * 3] * 3
>>> weird_board
[['_', '_', '_'], ['_', '_', '_'], ['_', '_', '_']]
>>> weird_board[1][2] = 'O'
>>> weird_board
[['_', '_', 'O'], ['_', '_', 'O'], ['_', '_', 'O']]
```

- This one is really interesting... Look at error, so it managed to extend the list but as it's a tuple you can't change it's value so it fails.
```py
>>> t = (1, 2, [30, 40])
>>> t[2] += [50, 60]
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
>>> t
(1, 2, [30, 40, 50, 60])

• Avoid putting mutable items in tuples.
• Augmented assignment is not an atomic operation—we just saw it throwing an
exception after doing part of its job.
• Inspecting Python (using dis("code")) bytecode is not too difficult, and can be helpful to see what is going on under the hood.
```

- **list.sort**

Both **list.sort** and **sorted** take two optional, keyword-only arguments:
**reverse**
    If True, the items are returned in descending order (i.e., by reversing the comparison of the items). The default is False.
**key**
    A one-argument function that will be applied to each item to produce its sorting key.

- bisect.insort function, which you can use to make
sure that your sorted sequences stay sorted

- **Memoryview**

```py
octets = array('B', range(6))
>>> m1 = memoryview(octets)
>>> m1.tolist()
[0, 1, 2, 3, 4, 5]
>>> m2 = m1.cast('B', [2, 3])
>>> m2.tolist()
[[0, 1, 2], [3, 4, 5]]
>>> m3 = m1.cast('B', [3, 2])
>>> m3.tolist()
[[0, 1], [2, 3], [4, 5]]
>>> m2[1,1] = 22
>>> m3[1,1] = 33
>>> octets
array('B', [0, 1, 2, 33, 22, 5])
```

## Array under type (typecodes):

'b' signed char (1x) or 'B' for unsigned
'h' signed short (2x)
'i' signed int (2x)
'l' signed long (4x)
'q' signed long long (8x)
'f' float
'd' double

change memory under
```py
>>> numbers = array.array('h', [-2, -1, 0, 1, 2])
>>> memv = memoryview(numbers)
>>> len(memv)
5
>>> memv[0]
-2
>>> memv_oct = memv.cast('B')
>>> memv_oct.tolist()
[254, 255, 255, 255, 0, 0, 1, 0, 2, 0]
>>> memv_oct[5] = 4
>>> numbers
array('h', [-2, -1, 1024, 1, 2])
```

- **DICT**

- Python is basically dicts wrapped in loads of syntactic sugar.

```py
A clever hook in the mapping API is the __missing__ method, which lets you customize what happens when a key is not found when using the d[k] syntax that invokes __getitem__.

(extend from dict and create your own class implementing get etc..)
```

- Dictionary views were a great addition in Python 3, eliminating the memory overhead of the Python 2 **.keys()**, **.values()**, and **.items()** methods that built lists duplicating data in the target dict instance. In addition, the dict_keys and dict_items classes support the most useful operators and methods of frozenset (frozenset([iterator]) is immutable set())


```py
@dataclass(*, init=True, repr=True, eq=True, order=False,
unsafe_hash=False, frozen=False)

examples:

from dataclasses import dataclass, field

@dataclass
class ClubMember:
    name: str
    guests: list = field(default_factory=list)

#WITHOUT the field
ValueError: mutable default <class 'list'> for field guests is not allowed:
use default_factory

@dataclass
class ClubMember:
    name: str
    guests: list = field(default_factory=list)
    athlete: bool = field(default=False, repr=False
```

- To this there is also: collections.namedtuple (old), (same but new have to say type) typing.NamedTuple

- **Choosing Between == and is**
The == operator (__eq__) compares the values of objects (the data they hold), while is compares their identities (id memory address). The is operator is faster than == as no overloads (calling of methods can happen).

-  l2 = l1[:] same as l2 = list(l1)
  
```py
>>> def f(a, b):
... a += b
... return a
...
>>> x = 1
>>> y = 2
>>> f(x, y)
3
>>> x, y
(1, 2)
>>> a = [1, 2]
>>> b = [3, 4]
>>> f(a, b)
[1, 2, 3, 4]
>>> a, b
([1, 2, 3, 4], [3, 4])
>>> t = (10, 20)
>>> u = (30, 40)
>>> f(t, u)
(10, 20, 30, 40)
>>> t, u
((10, 20), (30, 40))
Because Tuples are immutable unlike lists 
```

## Mutable Types as **Parameter Defaults: Bad Idea**

```py
>>> class HauntedBus:
"""A bus model haunted by ghost passengers"""
def __init__(self, passengers=[]): # BIG NO-NO
    self.passengers = passengers
def pick(self, name):
    self.passengers.append(name)
def drop(self, name):
    self.passengers.remove(name)

>>> bus1 = HauntedBus(['Alice', 'Bill'])
>>> bus1.passengers
['Alice', 'Bill']
>>> bus1.pick('Charlie')
>>> bus1.drop('Alice')
>>> bus1.passengers
['Bill', 'Charlie']
>>> bus2 = HauntedBus()
>>> bus2.pick('Carrie')
>>> bus2.passengers
['Carrie']
>>> bus3 = HauntedBus()
>>> bus3.passengers
['Carrie']
>>> bus3.pick('Dave')
>>> bus2.passengers
['Carrie', 'Dave']
>>> bus2.passengers is bus3.passengers
True
>>> bus1.passengers
['Bill', 'Charlie']

```

**Additionally** (THIS I DIDN'T KNOW):

```py
>>> basketball_team = ['Sue', 'Tina', 'Maya', 'Diana', 'Pat']
>>> bus = TwilightBus(basketball_team)
>>> bus.drop('Tina')
>>> bus.drop('Pat')
>>> basketball_team
['Sue', 'Maya', 'Diana']
```

- POLA https://en.wikipedia.org/wiki/Principle_of_least_astonishment

- **DEL** The most misunderstood of Python’s statements
del deletes references, not object

```py
>>> a = [1, 2]
>>> b = a
>>> del a
>>> b
[1, 2]
>>> b = [3]
```

- **__del__** is invoked by the Python interpreter when the instance is
**about to be destroyed** to give it a chance to release external resource

##  t is a tuple = (1, 2, 3), so ** t[:] and tuple(t) RETURN THE SAME REF! lol... **
Same works for str, bytes, and frozenset (immutable set...).

- LOOK THIS:

```py
>>> t1 = (1, 2, 3)
>>> t3 = (1, 2, 3)
>>> t3 is t1
False
>>> s1 = 'ABC'
>>> s2 = 'ABC'
>>> s2 is s1
True
```
optimization technique called **interning** at play Never use is, always == for str (bytes most likely)

- **FUNCTIONS (first class)**

```py
>>> def factorial(n):
... """returns n!"""
... return 1 if n < 2 else n * factorial(n - 1)
...
>>> factorial(42)
1405006117752879898543142606244511569936384000000000
>>> factorial.__doc__
'returns n!'
>>> type(factorial)
<class 'function'>

>>> fact(5)
120
>>> map(factorial, range(11))
<map object at 0x...>
>>> list(map(factorial, range(11)))
[1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800]
```

- A function that takes a function as an argument or returns a function as the result is a **higher-order function**.
  
```py
>>> fruits = ['strawberry', 'fig', 'apple', 'cherry', 'raspberry', 'banana']
>>> sorted(fruits, key=len)
['fig', 'apple', 'cherry', 'banana', 'raspberry', 'strawberry']
```

```py
>>> def reverse(word):
...     return word[::-1]
>>> reverse('testing')
'gnitset'
>>> sorted(fruits, key=reverse)
['banana', 'apple', 'fig', 'raspberry', 'strawberry', 'cherry']
```

- In the functional programming paradigm, some of the best known higher-order functions are **map, filter, reduce,** and (NO MORE) apply (fn(*args, **kwargs) instead of apply(fn, args, kwargs).) 
P.S. * => any iterable; ** =>  dictionary UNPACKING

- **DON'T USE MAP & FILTER**

```py
>>> list(map(factorial, range(6)))
[1, 1, 2, 6, 24, 120]
>>> [factorial(n) for n in range(6)]
[1, 1, 2, 6, 24, 120]
>>> list(map(factorial, filter(lambda n: n % 2, range(6))))
[1, 6, 120]
>>> [factorial(n) for n in range(6) if n % 2]
[1, 6, 120]
>>>
```

## FUNCTOOLS
Reduce not anymore part of Python:

```py
>>> from functools import reduce
>>> from operator import add
>>> reduce(add, range(100))
4950
>>> sum(range(100))
4950
```

- Lambda just not worth it...

## \_\_call\_\_ impl to make usage of obj()

```py
>>> bingo = BingoCage(range(3)) # bingo impls __call__
>>> bingo.pick()
1
>>> bingo()
0
>>> callable(bingo)
True
```

```py
def tag(name, *content, class_=None, **attrs):
    """Generate one or more HTML tags"""
    if class_ is not None:
        attrs['class'] = class_
        attr_pairs = (f' {attr}="{value}"' for attr, value
                                           in sorted(attrs.items()))
        attr_str = ''.join(attr_pairs)
    if content:
        elements = (f'<{name}{attr_str}>{c}</{name}>'
                    for c in content)
        return '\n'.join(elements)
    else:
        return f'<{name}{attr_str} />'
```

```py
>>> tag('br')
'<br />'
>>> tag('p', 'hello')
'<p>hello</p>'
>>> print(tag('p', 'hello', 'world'))
<p>hello</p>
<p>world</p>
>>> tag('p', 'hello', id=33)
'<p id="33">hello</p>'
>>> print(tag('p', 'hello', 'world', class_='sidebar'))
<p class="sidebar">hello</p>
<p class="sidebar">world</p>
>>> tag(content='testing', name="img")
'<img content="testing" />'
>>> my_tag = {'name': 'img', 'title': 'Sunset Boulevard',
... 'src': 'sunset.jpg', 'class': 'framed'}
>>> tag(**my_tag)
'<img class="framed" src="sunset.jpg" title="Sunset Boulevard" />'
```

- Have to be a named param, before must do *
```py
>>> def f(a, *, b):
... return a, b
...
>>> f(1, b=2)
(1, 2)
>>> f(1, 2)
TypeError: f() takes 1 positional argument but 2 were given
```

And reverse this is /, must be positional args:
```py
def divmod(a, b, /):
    return (a // b, a % b)
```

- The operator module provides function equivalents for dozens of operators so you don’t have to code trivial functions like lambda a, b: a*b. but do from operator import mul

Ohh this is just funny, in operator you can find following:
- **operator.itemgetter, operator.attrgetter, operator.methodcaller**

```py
>>> from operator import mul
>>> from functools import **partial**
>>> triple = partial(mul, 3)
>>> triple(7)
21
>>> list(map(triple, range(1, 10)))
[3, 6, 9, 12, 15, 18, 21, 24, 27]

or better

nfc = functools.partial(unicodedata.normalize, 'NFC')
>>> s1 = 'café'
>>> s2 = 'cafe\u0301'
>>> s1, s2
('café', 'café')
>>> s1 == s2
False
>>> nfc(s1) == nfc(s2)
True
```

- **Expressions cannot contain Statements** 
Expressions produce a value, and that value will be passed into the function. Statements don't produce a value, and so they can't be used as function arguments.

- Python will remain a dynamically typed language,
and the authors have no desire to ever make type hints mandatory, even by
convention

- **MYPY**
```py
tuple[int, ...] is a tuple with int items
list[tuple[str, ...]]
```

```py
from collections.abc import Mapping
def name2hex(name: str, color_map: Mapping[str, int]) -> str:
Using abc.Mapping allows the caller to provide an instance of dict, defaultdict, ChainMap, a UserDict subclass, or any other type that is a subtype-of Mapping.

instead of: def name2hex(name: str, color_map: dict[str, int]) -> str:

def fsum(__seq: Iterable[float]) -> float:
```

- And how to do generics:

```py
from collections.abc import Sequence
from random import shuffle
from typing import TypeVar
T = TypeVar('T')

def sample(population: Sequence[T], size: int) -> list[T]:
    if size < 1:
        raise ValueError('size must be >= 1')
    result = list(population)
    shuffle(result)
    return result[:size]
```

- **Decorators (syntactic sugar)**
A decorator is a callable that takes another function as an argument (the decorated function).

A decorator may perform some processing with the decorated function, and returns it or replaces it with another function or callable object.
(replace function with class => class decorator)

```py
@decorate
def target():
    print('running target()')

same as:

def target():
    print('running target()')
target = decorate(target)

```

usually it replaces func with different one:

>>> def deco(func):
...     def inner():
...         print('running inner()')
...     return inner
...
>>> @deco
... def target():
...     print('running target()')
...
>>> target()
running inner()
>>> target
<function deco.<locals>.inner at 0x10063b598>

- When you load module, decorators are executed...

- Lol so it's not only interpreting line by line, it analyses func...

```py
>>> b = 6
>>> def f2(a):
... print(a)
... print(b)
... b = 9
...
>>> f2(3)
3
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
File "<stdin>", line 3, in f2
UnboundLocalError: local variable 'b' referenced before assignment
```
So it outputs 3, first print(a) however it wont do print(b) but we have global var...

- This is much better than the behavior of JavaScript, which does not require variable declarations either, but if you do forget to declare that a variable is local (with var), you may clobber a global variable without knowing.

```py
>>> b = 6
>>> def f3(a):
...     **global** b
...     print(a)
...     print(b)
...     b = 9
...
>>> f3(3)
3
6
>>> b
9
```

- **closure**

```py
def make_averager(): # func body is a closure
    series = []
        def averager(new_value):
            series.append(new_value) # series is a free variable
            total = sum(series)
            return total / len(series)
    return averager
```

- **Closure** is a function that retains the bindings of the free variables
that exist when the function is defined, so that they can be used later when the function is invoked and the defining scope is no longer available

## **The nonlocal Declaration**

```py
def make_averager():
    count = 0
    total = 0
        def averager(new_value):
            # nonlocal count, total
            # without nonlocal: UnboundLocalError: local variable 'count' referenced before assignment
            count += 1
            total += new_value
            return total / count
    return averager

```

So it's same logic as for global variable, if you try to assign into them (=) then you need to say something because by default Python assumes local

-  **nonlocal** It lets you declare a variable as a free variable even when it is assigned within the function

## **Variable Lookup Logic**
1) If there is a global x declaration, x comes from and is assigned to the x global variable module (no global global just module global).
2) If there is a nonlocal x declaration, x comes from and is assigned to the x local variable of the nearest surrounding function where x is defined.
3) If x is a parameter or is assigned a value in the function body, then x is the local variable.
4) If x is referenced but is not assigned and is not a parameter:
    - x will be looked up in the local scopes of the surrounding function bodies (nonlocal scopes).
    - If not found in surrounding scopes, it will be read from the module global scope.
    - If not found in the global scope, it will be read from __builtins__.__dict__

## Implementing a Simple Decorator

```py
import time

def clock(func):
    def clocked(*args):
        t0 = time.perf_counter()
        result = func(*args) # func is a free variable
        elapsed = time.perf_counter() - t0
        name = func.__name__
        arg_str = ', '.join(repr(arg) for arg in args)
        print(f'[{elapsed:0.8f}s] {name}({arg_str}) -> {result!r}')
        return result
    return clocked
```

- The clock decorator implemented in Example 9-14 has a few shortcomings: it does not support keyword arguments, and it masks the __name__ and __doc__ of the decorated function

```py
import time
import functools
def clock(func):
    @functools.wraps(func)
    def clocked(*args, **kwargs):
        t0 = time.perf_counter()
        result = func(*args, **kwargs)
        elapsed = time.perf_counter() - t0
        name = func.__name__
        arg_lst = [repr(arg) for arg in args]
        arg_lst.extend(f'{k}={v!r}' for k, v in kwargs.items())
        arg_str = ', '.join(arg_lst)
        print(f'[{elapsed:0.8f}s] {name}({arg_str}) -> {result!r}')
        return result
    return clocked

# functools.wraps decorator to copy the relevant attributes from func to clocked.
```

## Std lib decorators

## Memoization with functools.cache

```py
import functools
from clockdeco import clock
@functools.cache
@clock
def fibonacci(n):
    if n < 2:
        return n
    return fibonacci(n - 2) + fibonacci(n - 1)

# stacking decorators
@alpha
@beta
def my_fn():
...
#same as:
my_fn = alpha(beta(my_fn))
```

## functools.lru_cache (cache with size, better memory then above)

```py
@lru_cache(maxsize=2**20, typed=True)
def costly_function(a, b):
```
types = true just makes different between f(1.0) and f(1) results (if false they are single entry)

## **Single Dispatch** Generic Functions (do different based on type, somewhat of a **method type overload** based on type of parameter)

- The functools.singledispatch decorator allows different modules to contribute to the overall solution, and lets you easily provide specialized functions even for types that belong to third-party packages that you can’t edit. 

- If you decorate a plain function with @singledispatch, it becomes the entry point for a generic function: a group of functions to perform the same operation in different ways, depending on the type of the first argument. This is what is meant by the term single dispatch. If more arguments were used to select the specific functions, we’d have multiple dispatch.

Do different based on type:
```py
from functools import singledispatch
from collections import abc
import fractions
import decimal
import html
import numbers

@singledispatch
def htmlize(obj: object) -> str:
    content = html.escape(repr(obj))
    return f'<pre>{content}</pre>'

@htmlize.register
def _(text: str) -> str:
    content = html.escape(text).replace('\n', '<br/>\n')
    return f'<p>{content}</p>'

@htmlize.register
def _(seq: abc.Sequence) -> str:
    inner = '</li>\n<li>'.join(htmlize(item) for item in seq)
    return '<ul>\n<li>' + inner + '</li>\n</ul>'

@htmlize.register
def _(n: numbers.Integral) -> str:
    return f'<pre>{n} (0x{n:x})</pre>'

@htmlize.register
def _(n: bool) -> str:
    return f'<pre>{n}</pre>'

@htmlize.register(fractions.Fraction) # type can be added in registry
def _(x) -> str:
    frac = fractions.Fraction(x)
    return f'<pre>{frac.numerator}/{frac.denominator}</pre>'

@htmlize.register(decimal.Decimal) # if _ then stack multi
@htmlize.register(float)
def _(x) -> str:
    frac = fractions.Fraction(x).limit_denominator()
    return f'<pre>{x} ({frac.numerator}/{frac.denominator})</pre>'
```

- https://peps.python.org/pep-0443/

## Parameterized Decorators

```py
registry = []
def register(func):
    print(f'running register({func})')
    registry.append(func)
    return func

@register
def f1():
    print('running f1()')

print('running main()')
print('registry ->', registry)
f1()

```

```py
registry = set()
def register(active=True):
    def decorate(func):
        print('running register'
        f'(active={active})->decorate({func})')
        if active:
            registry.add(func)
        else:
            registry.discard(func)
        return func
    return decorate

@register(active=False)
def f1():
print('running f1()')
@register()
def f2():
print('running f2()')
def f3():
print('running f3()')

same as

registry(active=False)(f1)
registry()(f2)
```

## A Class-Based Clock Decorator

```py

import time

DEFAULT_FMT = '[{elapsed:0.8f}s] {name}({args}) -> {result}'

class clock:
    
    def __init__(self, fmt=DEFAULT_FMT):
        self.fmt = fmt

    def __call__(self, func):
        def clocked(*_args):
            t0 = time.perf_counter()
            _result = func(*_args)
            elapsed = time.perf_counter() - t0
            name = func.__name__
            args = ', '.join(repr(arg) for arg in _args)
            result = repr(_result)
            print(self.fmt.format(**locals()))
            return _result
        return clocked
```

- Good examples: https://github.com/GrahamDumpleton/wrapt/blob/develop/blog/README.md

# **Conformity to patterns is not a measure of goodness**.
—Ralph Johnson, coauthor of the Design Patterns classic

-globals() Return a dictionary representing the current global symbol table

```py
Promotion = Callable[[Order], Decimal]
promos: list[Promotion] = []
def promotion(promo: Promotion) -> Promotion:
    promos.append(promo)
    return promo

def best_promo(order: Order) -> Decimal:
    """Compute the best discount available"""
    return max(promo(order) for promo in promos)
    
@promotion
def fidelity(order: Order) -> Decimal:
    """5% discount for customers with 1000 or more fidelity points"""
    if order.customer.fidelity >= 1000:
        return order.total() * Decimal('0.05')
    return Decimal(0)

@promotion
def bulk_item(order: Order) -> Decimal:
    """10% discount for each LineItem with 20 or more units"""
    discount = Decimal(0)
    for item in order.cart:
        if item.quantity >= 20:
            discount += item.total() * Decimal('0.1')
    return discount

@promotion
def large_order(order: Order) -> Decimal:
    """7% discount for orders with 10 or more distinct items"""
    distinct_items = {item.product for item in order.cart}
    if len(distinct_items) >= 10:
        return order.total() * Decimal('0.07')
    return Decimal(0)
```


```py
class MacroCommand:
    """A command that executes a list of commands"""
    def __init__(self, commands):
        self.commands = list(commands)
        
    def __call__(self):
        for command in self.commands:
            command()
```

## Objects & stuff

```py
class MyClass:
...     def myPublicMethod(self):
...             print 'public method'
...     def __myPrivateMethod(self):
...             print 'this is private!!'
...
>>> obj = MyClass()

>>> obj.myPublicMethod()
public method

>>> obj.__myPrivateMethod()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: MyClass instance has no attribute '__myPrivateMethod'

>>> dir(obj)
['_MyClass__myPrivateMethod', '__doc__', '__module__', 'myPublicMethod']

>>> obj._MyClass__myPrivateMethod()
this is private!!

```

- The __repr__ special method is called by the repr built-in to get the string representation of the object for inspection. For f"{obj!r}" there are three conversion flags are currently supported: '!s' which calls str() on the value, '!r' which calls repr() and '!a' which calls ascii().

- By default, an object is considered true unless its class defines either a __bool__() method that returns False or a __len__() method that returns zero, when called with the object. 1 Here are most of the built-in objects considered false:

    constants defined to be false: None and False

    zero of any numeric type: 0, 0.0, 0j, Decimal(0), Fraction(0, 1)

    empty sequences and collections: '', (), [], {}, set(), range(0)

- Look at all of this... So many function, interesting bytes and format.
- x,y = my_vector enabled by __iter__
- eq enables comparison with other iterables containing 2 things..
- Using eval shows that the repr of a Vector2d is a faithful representation of its constructor call.2
- bytes uses the __bytes__ method to produce a binary representation

```py
from array import array
import math

class Vector2d:
    typecode = 'd'

    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __iter__(self):
        return (i for i in (self.x, self.y))

    def __repr__(self):
        class_name = type(self).__name__
        return '{}({!r}, {!r})'.format(class_name, *self)
    
    def __str__(self):
        return str(tuple(self))
    
    def __bytes__(self):
        return (bytes([ord(self.typecode)]) + bytes(array(self.typecode, self)))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __hash__(self):
        return hash((self.x, self.y))
    
    def __abs__(self):
        return math.hypot(self.x, self.y)
    
    def __bool__(self):
        return bool(abs(self))

    def __format__(self, fmt_spec=''):
        components = (format(c, fmt_spec) for c in self)
        return '({}, {})'.format(*components)

```

- Usage
  
```py
>>> v1 = Vector2d(3, 4)
>>> print(v1.x, v1.y)
3.0 4.0
>>> x, y = v1
>>> x, y
(3.0, 4.0)
>>> v1
Vector2d(3.0, 4.0)
>>> v1_clone = eval(repr(v1))
>>> v1 == v1_clone
True
>>> print(v1)
(3.0, 4.0)
>>> octets = bytes(v1)
>>> octets
b'd\\x00\\x00\\x00\\x00\\x00\\x00\\x08@\\x00\\x00\\x00\\x00\\x00\\x00\\x10@'
>>> abs(v1)
5.0
>>> bool(v1), bool(Vector2d(0, 0))
(True, False)
```

- classmethod Versus staticmethod

```py
# Inside Vector2d class
@classmethod
def frombytes(cls, octets):
    typecode = chr(octets[0])
    memv = memoryview(octets[1:]).cast(typecode)
    return cls(*memv)

```

- Also there is @staticmethod but thats just not that useful really, as it's the same as being outside in module..

## class pattern matching

To make Vector2d work with positional patterns, we need to add a class attribute
named __match_args__ , listing the instance attributes in the order they will be used
for positional pattern matching:
class Vector2d:
__match_args__ = ('x', 'y')

```py
def positional_pattern_demo(v: Vector2d) -> None:
    match v:
        case Vector2d(0, 0):
            print(f'{v!r} is null')
        case Vector2d(0):
            print(f'{v!r} is vertical')
        case Vector2d(_, 0):
            print(f'{v!r} is horizontal')
        case Vector2d(x, y) if x==y:
            print(f'{v!r} is diagonal')
        case _:
            print(f'{v!r} is awesome')
```

## READ ONLY PROPS
```py
class Vector2d:
    typecode = 'd'

    def __init__(self, x, y):
        self.__x = float(x) #__x is "private". it's _Vector2d__x
        self.__y = float(y) #__y is "private", it's _Vector2d__y
    # The private is not real private, it's more put in place for subclass (name mangling)
    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    ...
```

## Saving Memory with __slots__

```py
>>> class Pixel:
... __slots__ = ('x', 'y')
...
>>> p = Pixel()
>>> p.__dict__
Traceback (most recent call last):
...
AttributeError: 'Pixel' object has no attribute '__dict__'
>>> p.x = 10
>>> p.y = 20
>>> p.color = 'red'
Traceback (most recent call last):
...
AttributeError: 'Pixel' object has no attribute 'color'

```

- __slots__ must be present when the class is created; adding or changing it later has no effect. The attribute names may be in a tuple or list, but I prefer a tuple to make it clear there’s no point in changing it.

## The __slots__ class attribute may provide significant memory savings if properly used, but there are a few caveats

- you must remember to redeclare __slots__ in each subclass to prevent their
instances from having __dict__.

- instances will only be able to have the attributes listed in __slots__, unless you include '__dict__' in __slots__ (but doing so may negate the memory savings).

- classes using __slots__ cannot use the @cached_property decorator, unless
they explicitly name '__dict__' in __slots__.

- Instances cannot be targets of weak references, unless you add '__weakref__' in __slots__.

- A primary use for **weak references** is to implement caches or mappings holding large objects, where it’s desired that a large object not be kept alive solely because it appears in a cache or mapping.

- NEW IMPL

```py
from array import array
import reprlib
import math
class Vector:
    typecode = 'd'

    def __init__(self, components):
        self._components = array(self.typecode, components)
    
    def __iter__(self):
        return iter(self._components)
    
    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find('['):-1]
        return f'Vector({components})'

    def __str__(self):
        return str(tuple(self))
        
    def __bytes__(self):
        return (bytes([ord(self.typecode)]) + bytes(self._components))

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for a, b in zip(self, other):
            if a != b:
            return False
        return True

    or

    def __eq__(self, other):
    return len(self) == len(other) and all(a == b for a, b in zip(self, other))
    
    def __abs__(self):
        return math.hypot(*self)

    def __bool__(self):
        return bool(abs(self))

    @classmethod
    def frombytes(cls, octets):
        typecode = chr(octets[0])
        memv = memoryview(octets[1:]).cast(typecode)
        return cls(memv)

```

- S.indices(len) -> (start, stop, stride)
Assuming a sequence of length len, calculate the start and stop indices, and the stride length of the extended slice described by S. Out-of-bounds indices are clipped just like they are in a normal slice.

additional to vector:

```py
def __len__(self):
    return len(self._components)

def __getitem__(self, key):
    if isinstance(key, slice):
        cls = type(self)
        return cls(self._components[key])
    index = operator.index(key)
    return self._components[index]

naive impl would be (because of types not checked)

def __getitem__(self, index):
    return self._components[index]

```

- Naive impl has two problems:
1) You can do a [3.14] -> solved by .index(key)
2) Slice return array instead of Vector

## __getattr__ And again here is something you should never use:

```py
__match_args__ = ('x', 'y', 'z', 't')
def __getattr__(self, name):
    cls = type(self)
    try:
        pos = cls.__match_args__.index(name)
    except ValueError:
        pos = -1
    
    if 0 <= pos < len(self._components):
        return self._components[pos]
    
    msg = f'{cls.__name__!r} object has no attribute {name!r}'
    raise AttributeError(msg)

```
Instead of impl 4x @propery get X get Y etc... By the way, __getattr__ works only after object doesn't have defined field

## The super() function provides a way to access methods of superclasses dynamically

## ABC (Abstract Base Class)

```py
import abc
class Tombola(abc.ABC):

    @abc.abstractmethod
    def load(self, iterable):
        """Add items from an iterable."""

    @abc.abstractmethod
    def pick(self):
        """Remove item at random, returning it.
        This method should raise `LookupError` when the instance is empty.
        """

    def loaded(self):
        """Return `True` if there's at least 1 item, `False` otherwise."""
        return bool(self.inspect())

    def inspect(self):
        """Return a sorted tuple with the items currently inside."""
        items = []
        while True:
            try:
                items.append(self.pick())
            except LookupError:
                break

        self.load(items)
        return tuple(items)
```

## Goose typing above 

## LOOK AT static duck typing (From golang..)

## LOOK AT ABCs Are Mixins Too

## Static typing

- I learned a painful lesson that for small programs, dynamic typing is great. For large programs you need a more disciplined approach. And it helps if the language gives you that discipline rather than telling you “Well, you can do whatever you want”.

## Overloading stuff...

```py
def __add__(self, other):
pairs = itertools.zip_longest(self, other, fillvalue=0.0)
return Vector(a + b for a, b in pairs)

```

without problems

```py
def __add__(self, other):
    try:
        pairs = itertools.zip_longest(self, other, fillvalue=0.0)
        return Vector(a + b for a, b in pairs)
    except TypeError:
        return NotImplemented

def __radd__(self, other):
    return self + other
```

-  For instance, to evaluate the expression x - y, where y is an instance of a class that has an rsub() method, y.rsub(x) is called if x.sub(y) returns NotImplemented. So supporting 4 + myobj (if myobj only has add, then 4 + myobj will fail, because 4 doesn't know about myobj...)

## Using @ as an Infix Operator

```py
class Vector:
# many methods omitted in book listing
def __matmul__(self, other):
    if (isinstance(other, abc.Sized) and
        isinstance(other, abc.Iterable)):
    if len(self) == len(other):
        return sum(a * b for a, b in zip(self, other))
    else:
        raise ValueError('@ requires vectors of equal length.')
    else:
        return NotImplemented

def __rmatmul__(self, other):
    return self @ other

```

- **The iter built-in function:**
  1) Checks whether the object implements \_\_iter\_\_, and calls that to obtain an iterator.
  2) If \_\_iter\_\_ is not implemented, but \_\_getitem\_\_ is, then iter() creates an iterator that tries to fetch items by index, starting from 0(zero).
  3) If that fails, Python raises TypeError, usually saying 'C' object is not iterable, where C is the class of the target object

- iter(x) also considers the legacy __getitem__ method, while the Iterable ABC does not.

iterator will call read64 until it returns b'' (it's an open question how fast is this...)
```py
with open('mydata.db', 'rb') as f:
    read64 = partial(f.read, 64)
    for block in iter(read64, b''):
        process_block(block)

```

- From something thats iterable you build an iterator (has __next__ and __iter__ return self)

```py
import re
import reprlib
RE_WORD = re.compile(r'\w+')
class Sentence:
    def __init__(self, text):
        self.text = text
        self.words = RE_WORD.findall(text)
    
    def __repr__(self):
        return f'Sentence({reprlib.repr(self.text)})'
    
    def __iter__(self):
        return SentenceIterator(self.words)
    
class SentenceIterator:
    def __init__(self, words):
        self.words = words
        self.index = 0

    def __next__(self):
        try:
            word = self.words[self.index]
        except IndexError:
            raise StopIteration()
        self.index += 1
        return word

    def __iter__(self):
        return self
```

or 

```py
def __iter__(self):
    for word in self.words:
        yield word
```


## **Don’t Make the Iterable an Iterator for Itself**


- Generator objects implement the **Iterator interface**, so they are also iterable


- handling errors with float

```py
class ArithmeticProgression:
    def __init__(self, begin, step, end=None):
        self.begin = begin
        self.step = step
        self.end = end # None -> "infinite" series

    def __iter__(self):
        result_type = type(self.begin + self.step)
        result = result_type(self.begin)
        forever = self.end is None
        index = 0

        while forever or result < self.end:
            yield result
            index += 1
            # += would accumulate float error
            result = self.begin + self.step * index

# or replace all the above with simple funct...

def aritprog_gen(begin, step, end=None):
    result = type(begin + step)(begin)
    forever = end is None
    index = 0
    while forever or result < end:
        yield result
        index += 1
        result = begin + step * index
```


## itertools. count | takewhile | dropwhile | compress | zip_longest | chain (two or more iterables) | product (X) | groupby | cycle

Interesting impl of visualize subsclasses

## yield from 

```py
def tree(cls):
    yield cls.__name__, 0
    yield from sub_tree(cls, 1)


def sub_tree(cls, level):
    for sub_cls in cls.__subclasses__():
        yield sub_cls.__name__, level
        yield from sub_tree(sub_cls, level+1)


def display(cls):
    for cls_name, level in tree(cls):
        indent = ' ' * 4 * level
        print(f'{indent}{cls_name}')


if __name__ == '__main__':
    display(BaseException)

```

- A **coroutine** is really a generator function, created with the yield keyword in its body

## Coroutines are different from generators in that generators produce a series of values, whereas coroutines consume values and produce none or some result

- A generator is essentially a cut down (asymmetric) coroutine. The difference between a coroutine and generator is that a coroutine can accept arguments after it's been initially called, whereas a generator can't.

- As with Tuples (from record to mutable lists) similar happen with Generators: 
  
-  The only sensible operation on a generator used as an iterator is to call next(it) directly or indirectly via for loops and other forms of iteration

```py
# The `readings` variable can be bound to an iterator
# or generator object that yields `float` items:
readings: Iterator[float]

# The `sim_taxi` variable can be bound to a coroutine
# representing a taxi cab in a discrete event simulation.
# It yields events, receives `float` timestamps, and returns
# the number of trips made during the simulation:
sim_taxi: Generator[Event, float, int]

Generator[YieldType, SendType, ReturnType]
```

Generator = Coroutine -> what a poor choice of wording..

```py
from collections.abc import Generator

def averager() -> Generator[float, float, None]:
    total = 0.0
    count = 0
    average = 0.0
    while True:
        term = yield average
        total += term
        count += 1
        average = total/count

usage:

>>> coro_avg = averager()
>>> next(coro_avg) # this is import to jump start everything...
0.0
>>> coro_avg.send(10)
10.0
>>> coro_avg.send(30)
20.0
>>> coro_avg.send(5)
15.0

>>> coro_avg.send(20)
16.25
>>> coro_avg.close()
>>> coro_avg.close()
>>> coro_avg.send(5)
Traceback (most recent call last):
...
StopIteration
```

- averager() returns a generator that yields float values, accepts float values via .send(), and does not return a useful value.

• Generators produce data for iteration
• Coroutines are consumers of data
• To keep your brain from exploding, don’t mix the two concepts together
• Coroutines are not related to iteration
• Note: There is a use of having `yield` produce a value in a coroutine, but it’s not tied to iteration.

```py
from collections.abc import Generator
from typing import  NamedTuple

class Result(NamedTuple):
    count: int # type: ignore
    average: float

class Sentinel:
    def __repr__(self):
    return f'<Sentinel>'

STOP = Sentinel()

def averager2(verbose: bool = False) -> Generator[None, float | Sentinel, Result]:
    total = 0.0
    count = 0
    average = 0.0
    while True:
        term = yield
        
        if verbose:
            print('received:', term)
        if isinstance(term, Sentinel):
            break
        
        total += term
        count += 1
        average = total / count

    return Result(count, average)

Usage:
>>> coro_avg = averager2()
>>> next(coro_avg)
>>> coro_avg.send(10)
>>> coro_avg.send(30)
>>> coro_avg.send(6.5)
>>> try:
...     coro_avg.send(STOP)
... except StopIteration as exc:
...     result = exc.value
...
>>> result
Result(count=3, average=15.5)
```
- This idea of “smuggling” the return value out of the coroutine wrapped in a StopIteration exception is a bizarre hack

## Context manager object - **With**

- Context manager object

funny example of "mirror" reverse write

```py
import sys

class LookingGlass:

    def __enter__(self):
        self.original_write = sys.stdout.write
        sys.stdout.write = self.reverse_write
        return 'JABBERWOCKY'
    
    def reverse_write(self, text):
        self.original_write(text[::-1])
        
    def __exit__(self, exc_type, exc_value, traceback):
        sys.stdout.write = self.original_write
        if exc_type is ZeroDivisionError:
            print('Please DO NOT divide by zero!')
            return True
```

- **contextlib.closing**
A function to build context managers out of objects that provide a close()
method but don’t implement the __enter__/__exit__ interface.

- **contextlib.suppress**
A context manager to temporarily ignore exceptions given as arguments.

- **contextlib.nullcontext**
A context manager that does nothing, to simplify conditional logic around
objects that may not implement a suitable context manager. It serves as a standin when conditional code before the with block may or may not provide a context manager for the with statement—added in Python 3.7.

What a bunch of hacks...

- @contextmanager -> a function decorator, a generator, and the with
statement - Just give a function with a single yield, yea man hacks they use a fact that yield will stop executing...

```py
import contextlib
import sys

@contextlib.contextmanager
def looking_glass():
    original_write = sys.stdout.write

    def reverse_write(text):
        original_write(text[::-1])

    sys.stdout.write = reverse_write
    yield 'JABBERWOCKY' # after this come code in with
    sys.stdout.write = original_write
```

- @looking_glass() can be used...

```py
for item in my_list:
    if item.flavor == 'banana':
        break
else:
    raise ValueError('No banana flavor found')

try:
    dangerous_call()
except OSError:
    log('OSError...')
else:
    after_call()
```

## (EAFP) Easier to ask for forgiveness than permission - try catch

## (LBYL) Look before you leap - check flags before

## **Concurrency** is about dealing with lots of things at once.

## **Parallelism** is about doing lots of things at once.

- Concurrency provides a way to structure a solution to solve a problem that may (but not necessarily) be parallelizable

- Parallelism is a special case of concurrency

- All parallel systems are concurrent, but not all concurrent systems are parallel

- **Coroutine** - A function that can suspend itself and resume later

## async def, await, async with, and async for

- asynchronous context managers

## async def - native Coroutine await
Compared to classic Coroutine, instead of using yield you use await

## classic Coroutine - yield
A generator function that consumes data sent to it via my_coro.send(data) calls, and reads that data by using yield in an expression

## Generator-based coroutine - @types.coroutine (it can await)

## Asynchronous generator ->  async def and using yield in its body

```py
import asyncio
import socket
from keyword import kwlist

MAX_KEYWORD_LEN = 4

async def probe(domain: str) -> tuple[str, bool]:
    loop = asyncio.get_running_loop() # if no running loop throws
    try:
        await loop.getaddrinfo(domain, None)
    except socket.gaierror:
        return (domain, False)
    return (domain, True)

async def main() -> None:
    names = (kw for kw in kwlist if len(kw) <= MAX_KEYWORD_LEN)
    domains = (f'{name}.dev'.lower() for name in names)
    coros = [probe(domain) for domain in domains]

    for coro in asyncio.as_completed(coros):
        domain, found = await coro
        mark = '+' if found else ' '
        print(f'{mark} {domain}')

    if __name__ == '__main__':
        asyncio.run(main())
```

## **asyncio.as_completed** is a generator that yields coroutines that return the results of the coroutines passed to it in the order they are completed—not the order they were submitted

```py
import asyncio
import itertools


async def slow() -> int:
    await asyncio.sleep(3)
    return 42


async def spin(msg: str) -> None:
    # r means raw string (\ is just treated as \)
    # same as = '\\|/-'
    for char in itertools.cycle(r'\|/-'):
        status = f'\r{char} {msg}'
        print(status, flush=True, end='')
        try:
            await asyncio.sleep(.1)
        except asyncio.CancelledError:
            break
        blanks = ' ' * len(status)
        print(f'\r{blanks}\r', end='')


async def supervisor() -> int:
    spinner = asyncio.create_task(spin('thinking!'))
    print(f'spinner object: {spinner}')
    result = await slow()
    spinner.cancel()
    return result


def main() -> None:
    result = asyncio.run(supervisor())
    print(f'Answer: {result}')


if __name__ == '__main__':
    main()

```

## awaitables -> An object with an __await__ like asyncio.Task (sub of asyncio.Future)

asyncio.create_task(one_coro()) -> if you don't need result not need to wait, this scheduled a task for concurrent execution.

## You rewrite all your code so none of it blocks or you’re just wasting your time

- asyncio.as_completed (return each one as completes) vs gather (all)

## async with asyncio.Semaphore

- A semaphore can be held by multiple coroutines, with a configurable maximum number

## One important advantage of Node.js over Python for asynchronous programming is the Node.js standard library, which provides async APIs for all I/O—not just for network I/O

```py 
await asyncio.to_thread(save_flag, image, f'{cc}.gif')
```
- can produce hard-to-debug problems since cancellation doesn’t work the way one might expec

## Asynchronous Iteration and Asynchronous Iterables

async for...

- asynchronous iterator: An object that implements the __aiter__() and __anext__() methods. __anext__ must return an awaitable object. async for resolves the awaitables returned by an asynchronous iterator’s __anext__() method until it raises a StopAsyncIteration exception.

## Async Comprehensions and Async Generator Expressions

```py
import asyncio
 
# define an asynchronous generator
async def async_generator():
    # normal loop
    for i in range(10):
        # block to simulate doing work
        await asyncio.sleep(1)
        # yield the result
        yield i
 
# define a simple coroutine
async def custom_coroutine():
    # asynchronous for loop
    async for item in async_generator():
        # report the result
        print(item)
 
# start
asyncio.run(custom_coroutine())
```

```py
result = []
async for i in aiter():
    if i % 2:
        result.append(i)

    like this:
    result = [i async for i in aiter() if i % 2]

    In addition, given a native coroutine fun, we should be able to write this:
    result = [await fun() for fun in funcs]

```

- Using await in a list comprehension is similar to using asyn
cio.gather

## If you’re using Python at scale, you should have some automated tests designed specifically to detect performance regressions as soon as they appear

- Once you write your first async def, your program is inevitably going to
have more and more async def, await, async with, and async for. And using non-asynchronous libraries suddenly becomes a challenge.