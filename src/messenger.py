import json

# import logging
from client import Client

OK: bytes = "+OK\r\n".encode()
ERROR: bytes = "-ERROR\r\n".encode()


class SubKey:
    sub_key: bytes

    def __init__(self, subscriber: Client, sid: bytes) -> None:
        self.sub_key = subscriber.bId + sid
        pass

    def __eq__(self, __value: object) -> bool:
        return isinstance(__value, SubKey) and self.sub_key == __value.sub_key

    def __hash__(self) -> int:
        return hash(self.sub_key)


class Subscription:
    """Abstraction when a Client subsribes to a topic, and assign sid (subscription id) to it."""

    sid: bytes  # uniqueness handled by SubKey
    consumer: Client
    deleted: bool  # To amortize removal out of list
    msg_info_base: bytes

    def __init__(self, subscriber: Client, sid: bytes, topic: bytes) -> None:
        self.consumer = subscriber
        self.sid = sid
        self.deleted = False

        # MSG <subject> <sid> <#bytes>␍␊
        self.msg_info_base = b"MSG " + topic + b" " + sid + b" "
        pass

    def key(self) -> SubKey:
        return SubKey(self.consumer, self.sid)


class Messenger:
    """Multi-Client comunication via PUB and SUB based on NATS protocol.

    NATS Server<->Client flow:
    0) Server is waiting for Clients to connect on some port(default 4222) - TCP socket
    1) Client connect to TCP Socket (bidirectional comunication)
    2) Client may send CONNECT command to set options:
        CONNECT {"verbose":false}(CRLF)
    3) Client may send SUB command to subscribe to a Topic:
        - SUB sub_name SID(CRLF)
        *SID must be unique per Client
    4) Client may send PUB command to publish message to a Topic:
        - PUB sub_name BYTES_CNT(CRLF)
        - MSG(CRLF)
    5) Client may send UNSUB command to remove it-self from a Topic:
        - UNSUB SID
    """

    subscriptions: dict[bytes, list[Subscription]]
    sub_lookup: dict[SubKey, Subscription]
    subs_cnt: int
    subs_deleted: int

    clients_to_drain: list[Client]

    def __init__(self):
        self.subscriptions = {}
        self.sub_lookup = {}
        self.clients_to_drain = []
        self.subs_deleted = 0
        self.subs_cnt = 0

    def sub(self, consumer: Client, topic: bytes, sid: bytes) -> bytes:
        subscription = Subscription(consumer, sid, topic)
        key = subscription.key()

        if key in self.sub_lookup:
            if self.sub_lookup[key].deleted:  # want to subscribe again
                self.sub_lookup[key].deleted = False
                self.subs_cnt += 1
                return consumer.ack

            return ERROR

        if topic in self.subscriptions:
            self.subscriptions[topic].append(subscription)
        else:
            self.subscriptions[topic] = [subscription]

        self.sub_lookup[key] = subscription
        self.subs_cnt += 1

        return consumer.ack

    def _clean_deleted(self, rem_client_id: int):
        rem_topics: list[bytes] = []
        subs_cnt = 0

        for sub_key, subs in self.subscriptions.items():
            left_subs: list[Subscription] = []
            rem_keys: list[SubKey] = []

            for sub in subs:
                if sub.deleted:
                    rem_keys.append(sub.key())
                    continue

                if sub.consumer.id == rem_client_id:
                    rem_keys.append(sub.key())
                    continue

                left_subs.append(sub)

            if any(left_subs):
                self.subscriptions[sub_key] = left_subs
                subs_cnt += len(left_subs)
            else:
                rem_topics.append(sub_key)

            for k in rem_keys:
                del self.sub_lookup[k]

        for topic in rem_topics:
            del self.subscriptions[topic]

        self.subs_cnt = subs_cnt
        self.subs_deleted = 0

    def unsub(self, consumer: Client, sid: bytes) -> bytes:
        key = SubKey(consumer, sid)

        if key not in self.sub_lookup:
            return ERROR

        if self.sub_lookup[key].deleted:
            return ERROR

        self.sub_lookup[key].deleted = True
        self.subs_deleted += 1

        if self.subs_deleted > self.subs_cnt / 2:
            self._clean_deleted(-1)

        return consumer.ack

    def connect(self, client: Client, connect: bytes) -> bytes:
        connect_json = json.loads(connect)
        if "verbose" in connect_json:
            if connect_json["verbose"]:
                client.ack = OK
            else:
                client.ack = b""

            return client.ack

        return ERROR

    def disconnect(self, client: Client):
        self._clean_deleted(client.id)

    def publish_msg(
        self,
        client: Client,
        topic: bytes,
        b_cnt_bytes: bytes,
        msg: bytes,
    ) -> tuple[bytes, bool]:
        """MSG <subject> <sid> [reply-to] <#bytes>␍␊
        [payload]␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#msg

        Example:
            MSG FOO.BAR 9 11␍␊
            Hello World␍␊
        """
        if len(msg) != int(b_cnt_bytes):
            return ERROR, False

        if topic not in self.subscriptions:
            return client.ack, True

        for sub in self.subscriptions[topic]:
            if sub.deleted:
                continue

            if not any(sub.consumer.buffer):
                self.clients_to_drain.append(sub.consumer)

            sub.consumer.buffer.extend(
                [sub.msg_info_base, b_cnt_bytes, b"\r\n", msg, b"\r\n"]
            )

        return client.ack, True
