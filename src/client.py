import asyncio
import json


class IdsProvider:
    ids_to_reuse: list[int]
    max_id: int

    def __init__(self):
        self.ids_to_reuse = []
        self.max_id = 0

    def next(self) -> int:
        if len(self.ids_to_reuse) != 0:
            id = self.ids_to_reuse.pop()
            return id

        id = self.max_id
        self.max_id = self.max_id + 1
        return id

    def reuse(self, id: int):
        self.ids_to_reuse.append(id)


def int_to_bytes(x: int) -> bytes:
    return x.to_bytes((x.bit_length() + 7) // 8, "big")


class Client:
    """Client connected to TCP socket"""

    ip: str
    port: int

    id: int
    bId: bytes

    ack: bytes

    buffer: list[bytes]

    def __init__(self, id: int, ip: str, port: int, writer: asyncio.StreamWriter):
        self.id = id
        self.bId = int_to_bytes(id)
        self.ip = ip
        self.port = port
        self.writer = writer
        self.ack = b"+OK\r\n"
        self.buffer = []

    def info(self) -> bytes:
        """INFO {"option_name":option_value,...}␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#info

        Example(real NATS):
        INFO {
            "server_id": "NBAOOV7BMGKLDUROXWO3HZJSZVURN47KDMY65SSY36OTPV3OW4C37Q6E",
            "server_name": "NBAOOV7BMGKLDUROXWO3HZJSZVURN47KDMY65SSY36OTPV3OW4C37Q6E",
            "version": "2.10.7",
            "proto": 1,
            "git_commit": "fa8464d",
            "go": "go1.21.5",
            "host": "0.0.0.0",
            "port": 4222,
            "headers": true,
            "max_payload": 1048576,
            "client_id": 11,
            "client_ip": "172.18.0.1",
            "xkey": "XDLVYAVDJJSLJBPQ2NQTLS5TV46V6XPOSZJHVDK6WJQPU74NWPXX6F3T"
        }
        """
        info = {
            "server_id": "py_nats",
            "server_name": "py_nats",
            "version": "0.0.1",
            "proto": 1,
            "git_commit": "",
            "go": "",
            "host": "0.0.0.0",  # TODO
            "port": 4222,
            "headers": False,
            "max_payload": 1048576,
            "client_id": self.id,
            "client_ip": self.ip,
        }

        return ("INFO " + json.dumps(info) + "\r\n").encode()
