from messenger import Messenger, ERROR
from client import Client


class CmdsRunner:
    # goal is to remove PublishInfo all together...

    prev_cmd_pub_parts: list[bytes]

    def __init__(self) -> None:
        self.prev_cmd_pub_parts = []

    def process_batch(
        self, client: Client, nats: Messenger, cmds: list[bytes]
    ) -> bytes:
        """Process multiple bytes (cmds without /r/n at end)

        When cmds[i] is PUB then cmds[i+1] is Payload. Process both if possible.

        PUB <subject> <#bytes>␍␊
        [payload]␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#pub

        Examples:
        PUB FOO 11␍␊
        Hello NATS!␍␊

        """

        i: int = 0
        all_resp: list[bytes] = []

        if any(self.prev_cmd_pub_parts):
            topic, b_bytes_cnt = self.prev_cmd_pub_parts

            resp, _ = nats.publish_msg(
                client,
                topic,
                b_bytes_cnt,
                cmds[0],
            )
            all_resp.append(resp)
            i = 1
            self.prev_cmd_pub_parts = []

        cnt_cmds = len(cmds)
        while i < cnt_cmds - 1:
            # In case it's cmds[i] = PUB then cmds[i+1] = PUB Payload
            if cmds[i].startswith(b"PUB"):
                _, topic, b_bytes_cnt = cmds[i].split()

                resp, ok = nats.publish_msg(client, topic, b_bytes_cnt, cmds[i + 1])
                all_resp.append(resp)

                if ok:
                    i += 1
            else:
                all_resp.append(self._execute_next(client, nats, cmds[i].split()))

            i += 1
        if i == cnt_cmds - 1:
            all_resp.append(self._execute_next(client, nats, cmds[i].split()))

        return b"".join(all_resp)

    def _execute_next(
        self, client: Client, nats: Messenger, parts: list[bytes]
    ) -> bytes:
        """Processing each command in isolation, following is the list of NATS commands:

        PUB <subject> <#bytes>␍␊[payload]␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#pub

        SUB <subject> <sid>␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#sub

        UNSUB <sid>␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#unsub

        CONNECT {"option_name":option_value,...}␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#connect

        PING␍␊
        https://docs.nats.io/reference/reference-protocols/nats-protocol#ping-pong

        """
        match parts:
            case [b"PUB", topic, cnt_bytes] if cnt_bytes.isdigit():
                self.prev_cmd_pub_parts = [topic, cnt_bytes]
                return b""
            case [b"SUB", topic, sid]:
                return nats.sub(client, topic, sid)
            case [b"UNSUB", sid]:
                return nats.unsub(client, sid)
            case [b"CONNECT", *rest]:
                return nats.connect(client, b" ".join(rest))
            case [b"PING"]:
                return b"PONG\r\n"
            case _:
                return ERROR
