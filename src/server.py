"""Entry for py-nats Server.
Based on asyncio Sockets.
Starts Server and handles incoming connection via handle_new_socket function.
"""
import asyncio
from messenger import Messenger, ERROR
from client import Client, IdsProvider
from splitter import CommandsSplitter

import logging
import commands

# import traceback
# from pyinstrument import Profiler


READ_256KiB = 1024 << 8


async def handle_new_socket(
    nats: Messenger,
    ids_provider: IdsProvider,
    reader: asyncio.StreamReader,
    writer: asyncio.StreamWriter,
):
    addr = writer.get_extra_info("peername")
    ip: str = addr[0]
    port: int = addr[1]

    logging.debug(f"New connection {addr}")
    id = ids_provider.next()
    client = Client(id, ip, port, writer)

    writer.write(client.info())
    await writer.drain()

    cmd_splitter = CommandsSplitter()
    cmds_runner = commands.CmdsRunner()

    # p = Profiler()
    # p.start()

    while True:
        try:
            payload = await reader.read(READ_256KiB)

            if len(payload) == 0:
                raise Exception("no data")

            cmds = cmd_splitter.carry_over_split(payload.splitlines(), payload[-1])

            if len(cmds) == 0:
                writer.write(ERROR)
                await writer.drain()
                continue

            resp = cmds_runner.process_batch(client, nats, cmds)

            # write out content to subscribers if any
            for c in nats.clients_to_drain:
                c.writer.write(b"".join(c.buffer))
                c.buffer = []
            for c in nats.clients_to_drain:
                await c.writer.drain()
            nats.clients_to_drain = []

            if any(resp):
                client.writer.write(resp)
                await writer.drain()

        except:
            # p.stop()
            # print(p.output_text(unicode=True, color=True))
            # traceback.print_exc()
            writer.close()
            logging.debug(f"Disconnected from {addr!r}")
            nats.disconnect(client)
            ids_provider.reuse(client.id)
            break


async def start_server():
    nats = Messenger()
    ids_provider = IdsProvider()
    server = await asyncio.start_server(
        lambda r, w: handle_new_socket(nats, ids_provider, r, w),
        "127.0.0.1",
        4222,
    )

    addrs = ", ".join(str(sock.getsockname()) for sock in server.sockets)
    print(f"Serving on {addrs}")

    async with server:
        await server.serve_forever()


if __name__ == "__main__":
    logging.basicConfig(level="ERROR")
    asyncio.run(start_server())
