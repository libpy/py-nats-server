class CommandsSplitter:
    """Hold carry over bytes for next split to be combined.

    As are Commands separated by CR LF there are two special cases of carry over:
    1) CR LF bytes are carried over to next bucket of bytes read - carry over is command it self
    2) Only LF is carried over to next bucket of bytes to be read - carry over minus CR is command it self
    """

    carry_over: bytes

    def __init__(self) -> None:
        self.carry_over = b""

    def carry_over_split(self, lines: list[bytes], last_byte: int) -> list[bytes]:
        if len(self.carry_over) != 0:
            # lines[0] = b"" when current payload starts with /n or /r/n
            if lines[0] == b"":
                # means prev payload ended with /r or was full cmd
                lines[0] = self.carry_over
            else:
                lines[0] = self.carry_over + lines[0]

            self.carry_over = b""

        if any(lines):
            if last_byte != 10:  # if last is not /n
                self.carry_over = lines[-1]
                lines.pop()

        return lines
