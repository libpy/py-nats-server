from timeit import timeit
import numpy as np

PUB = b"PUB"
SUB = b"SUB"


def bytes_split_pubsub(payload: bytes, sub_pub_cnt: int):
    cmds: list[bytes] = payload.splitlines()

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""
    for cmd in cmds:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = cmd
            pub_next_to = b""
            continue

        split = cmd.split()
        if cmd.startswith(SUB):
            sub_topics.add(split[1])
        elif cmd.startswith(PUB):
            pub_next_to = split[1]
        else:
            raise Exception("unknown cmd")

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


CRLF = b"\r\n"


def find_indexes_pubsub(payload: bytes, sub_pub_cnt: int):
    cmd_ends: list[int] = []

    start = 0
    while True:
        next = payload.find(CRLF, start)
        if next == -1:
            break
        cmd_ends.append(next)
        start = next + 2

    parts: list[bytes] = payload.split()

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""

    parts_i = 0

    cmd_max_len = 0
    parts_len = 0
    for cmd_max_len in cmd_ends:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = parts[parts_i]
            pub_next_to = b""
            parts_len += len(parts[parts_i]) + 2
            parts_i += 1
            continue

        split: list[bytes] = []
        while parts_len - 1 != cmd_max_len:
            parts_len += len(parts[parts_i]) + 1
            split.append(parts[parts_i])
            parts_i += 1

        parts_len += 1

        if split[0] == SUB:
            sub_topics.add(split[1])
        elif split[0] == PUB:
            pub_next_to = split[1]
        else:
            raise Exception("unknown cmd")

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


def numpy_split_pubsub(np_payload, sub_pub_cnt: int):
    cmds = np.char.splitlines(np_payload)

    splits = np.char.split(cmds[0], b" ")

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""
    for split in splits:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = split[0]
            pub_next_to = b""
            continue

        if split[0] == SUB:
            sub_topics.add(split[1])
        elif split[0] == PUB:
            pub_next_to = split[1]
        else:
            raise Exception("unknown cmd")

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


def only_1xsplit_pubsub(payload: bytes, sub_pub_cnt: int):
    splits = payload.split()

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""

    consume_sub: bool = False
    consume_pub: bool = False

    for split in splits:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = split
            pub_next_to = b""
            continue

        if consume_sub:
            sub_topics.add(split)
            consume_sub = False
        elif consume_pub:
            pub_next_to = split
            consume_pub = False
        else:
            consume_sub = split == SUB
            consume_pub = split == PUB

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


def noglob_only_1xsplit_pubsub(payload: bytes, sub_pub_cnt: int):
    splits = payload.split()

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""

    consume_sub: bool = False
    consume_pub: bool = False

    for split in splits:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = split
            pub_next_to = b""
            continue

        if consume_sub:
            sub_topics.add(split)
            consume_sub = False
        elif consume_pub:
            pub_next_to = split
            consume_pub = False
        else:
            consume_sub = split == b"SUB"
            consume_pub = split == b"PUB"

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


def map_noglob_only_1xsplit_pubsub(payload: bytes, sub_pub_cnt: int):
    splits = payload.split()

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""

    consume_sub: bool = False
    consume_pub: bool = False

    for split in splits:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = split
            pub_next_to = b""
            continue

        if consume_sub:
            sub_topics.add(split)
            consume_sub = False
        elif consume_pub:
            pub_next_to = split
            consume_pub = False
        else:
            consume_sub = split == b"SUB"
            consume_pub = split == b"PUB"

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


b_payload = b""
sub_pub_cnt = 5000

for i in range(sub_pub_cnt):
    b_payload += f"SUB t_{i}\r\n".encode()
    b_payload += f"PUB t_{i}\r\n".encode()
    b_payload += b"qwertasdfgzxcvb\r\n"

np_payload = np.asarray([b_payload], dtype=np.bytes_)
br_payload = bytearray(b_payload)

v = bytes(br_payload[0:9])

print(f"start {len(b_payload)}")

only_1x_split_perf = timeit(
    stmt="only_1xsplit_pubsub(b_payload, sub_pub_cnt)",
    number=1000,
    globals=globals(),
)
print(only_1x_split_perf)

noglob_only_1x_split_perf = timeit(
    stmt="bytearray_only_1xsplit_pubsub(b_payload, sub_pub_cnt)",
    number=1000,
    globals=globals(),
)
print(noglob_only_1x_split_perf)


"""
split_perf = timeit(
    stmt="bytes_split_pubsub(b_payload, sub_pub_cnt)",
    number=1000,
    globals=globals(),
)
print(split_perf)



presplit_perf = timeit(
    stmt="find_indexes_pubsub(b_payload, sub_pub_cnt)",
    number=1000,
    globals=globals(),
)
print(presplit_perf)

np_split_perf = timeit(
    stmt="numpy_split_pubsub(np_payload, sub_pub_cnt)",
    number=1000,
    globals=globals(),
)
print(np_split_perf)
"""
