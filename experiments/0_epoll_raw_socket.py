import socket
import selectors
import types

HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
PORT = 65432  # Port to listen on (non-privileged ports are > 1023)

sel = selectors.DefaultSelector()

lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#TODO: check this if good and what it does really...
lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #reuse adr issue (only POSIX)

lsock.bind((HOST, PORT))
lsock.listen() #Num of scokets? 
print(f"Listening on {(HOST, PORT)}")
lsock.setblocking(False)

# selectors.EVENT_WRITE is remove because it's almost a busy wait then...
# HOWEVER, it should be checked... however healty socket is almost always ready to recieve!
sel.register(lsock, selectors.EVENT_READ, data=None) # initial socket

def accept_wrapper(sock):
    print(f"accept_wrapper{sock}")
    conn, addr = sock.accept()  # Should be ready to read
        
    print(f"Accepted connection from {addr}")
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, inb=b"", outb=b"")

    sel.register(conn, selectors.EVENT_READ, data=data)
    return conn

def service_connection(key, writter, listeners):
    print(f"service_connection: , w:{writter}, l:{listeners}")
    sock = key.fileobj
    data = key.data

    recv_data = sock.recv(1024)  # Should be ready to read
    if recv_data:
        if sock == writter:
            data.outb += recv_data
        else:
            print(f"NOT!: main:{writter}, sock:{sock}")
    else:
        print(f"Closing connection to {sock}")
        sel.unregister(sock)
        sock.close()
   
    if data.outb:
        sent = 0
        for l in listeners:
            print(f"Echoing {data.outb!r} to {l}")
            sent = l.send(data.outb)  # Should be ready to write
        data.outb = data.outb[sent:]

writter = None
listeners = []
try:
    i = 0
    while True:
        print(f"block{i}")
        events = sel.select(timeout=None) #blocking
        i = i + 1
        for key, mask in events:
            if key.data is None:
                newConnect = accept_wrapper(key.fileobj)
                if writter is None:
                    writter = newConnect
                else:
                    listeners.append(newConnect)
            else: # already accepter socket, need to "service" it's
                service_connection(key, writter, listeners)
except KeyboardInterrupt:
    print("Caught keyboard interrupt, exiting")
finally:
    sel.close()

