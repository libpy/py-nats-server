import asyncio

lock = asyncio.Lock()

producer = None
consumers = []

async def handle_new_socket(reader, writer):
    addr = writer.get_extra_info('peername')
    ip = addr[0]
    port = addr[1]
    print(f"Connected IP:{ip} PORT:{port}")

    async with lock:
        global producer
        if producer is None:
            print(f"Producer found: {addr}")
            producer = port
        else:
            global consumers
            print(f"New consumers: {addr}")
            consumers.append(writer)
    
    while True:
        try:
            # https://docs.python.org/3/library/asyncio-stream.html#asyncio.StreamReader.readline
            # TODO: Make this smarter so it doesn't explode
            data = await reader.readline()

            message = data[:len(data)-2].decode()
        
            print(f"Received {message!r} from {addr!r}")

            for w in consumers:
                cAddr = w.get_extra_info('peername')
                print(f"Sending {message!r} to {cAddr!r}")
                w.write(data)
                await w.drain()

        except: #when conection is terminated
            writer.close()
            await writer.wait_closed()
            print(f"Disconnected from {addr!r}")
            break

async def main():
    server = await asyncio.start_server(
        handle_new_socket, '127.0.0.1', 8888)

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}')

    async with server:
        await server.serve_forever()

asyncio.run(main())