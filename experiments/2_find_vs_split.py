from timeit import timeit


PUB = b"PUB"
SUB = b"SUB"


def bytes_split_pubsub(payload: bytes, sub_pub_cnt: int):
    cmds: list[bytes] = payload.splitlines()

    pub_topics: dict[bytes, bytes] = {}
    sub_topics: set[bytes] = set()

    pub_next_to = b""
    for cmd in cmds:
        if pub_next_to != b"":
            pub_topics[pub_next_to] = cmd
            pub_next_to = b""
            continue

        split = cmd.split()
        if cmd.startswith(SUB):
            sub_topics.add(split[1])
        elif cmd.startswith(PUB):
            pub_next_to = split[1]
        else:
            raise Exception("unknown cmd")

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


CRLF = b"\r\n"
SPACE = b" "
EMPTY_MEM = memoryview(b"")
PUB_MEM = memoryview(b"PUB")
SUB_MEM = memoryview(b"SUB")


def memview_pubsub(payload: bytes, sub_pub_cnt: int):
    cmd_ranges: list[tuple[int, int]] = []
    start = 0
    while True:
        next = payload.find(CRLF, start)
        if next == -1:
            break
        cmd_ranges.append((start, next))
        start = next + 2

    pub_topics: dict[memoryview, bytes] = {}
    sub_topics: set[memoryview] = set()

    mem_payload = memoryview(payload)
    pub_next_to: memoryview = EMPTY_MEM
    for cmd in cmd_ranges:
        if pub_next_to != EMPTY_MEM:
            pub_topics[pub_next_to] = payload[cmd[0] : cmd[1]]
            pub_next_to = EMPTY_MEM
            continue

        splits: list[memoryview] = []
        start = cmd[0]
        while True:
            next = payload.find(SPACE, start)
            if next == -1 or next > cmd[1]:
                splits.append(mem_payload[start : cmd[1]])
                break
            splits.append(mem_payload[start:next])
            start = next + 1

        if splits[0] == SUB_MEM:
            sub_topics.add(splits[1])
        elif splits[0] == PUB_MEM:
            pub_next_to = splits[1]
        else:
            raise Exception("unknown cmd")

    if len(pub_topics) != sub_pub_cnt:
        raise Exception(f"mem_pub_topics: {len(pub_topics)}!={sub_pub_cnt}")

    if len(sub_topics) != sub_pub_cnt:
        raise Exception(f"mem_sub_topics: {len(sub_topics)}!={sub_pub_cnt}")


b_payload = b""
sub_pub_cnt = 10000

for i in range(sub_pub_cnt):
    b_payload += f"SUB t_{i}\r\n".encode()
    b_payload += f"PUB t_{i}\r\n".encode()
    b_payload += b"qwertasdfgzxcvb\r\n"


split_perf = (
    timeit(
        stmt="bytes_split_pubsub(b_payload, sub_pub_cnt)",
        number=1000,
        globals=globals(),
    )
    / 100
)

print(f"SPLIT PERF: {split_perf}")


mem_perf = (
    timeit(
        stmt="memview_pubsub(b_payload, sub_pub_cnt)", number=1000, globals=globals()
    )
    / 100
)
print(f"MEM PERF: {mem_perf}")

print(f"MEM is {split_perf/mem_perf} times faster")
