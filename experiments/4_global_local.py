from timeit import timeit


PUB = b"PUB"


def count_pubs_global(parts: list[bytes]) -> int:
    count = 0
    for p in parts:
        if p == PUB:
            count += 1

    return count


def count_pubs(parts: list[bytes]) -> int:
    count = 0
    for p in parts:
        if p == b"PUB":
            count += 1

    return count


b_payload = b""
sub_pub_cnt = 5000

for i in range(sub_pub_cnt):
    b_payload += f"SUB t_{i}\r\n".encode()
    b_payload += f"PUB t_{i}\r\n".encode()
    b_payload += b"qwertasdfgzxcvb\r\n"

split_payload = b_payload.split()


print(count_pubs_global(split_payload))
print(count_pubs(split_payload))

glob_perf = timeit(
    stmt="count_pubs_global(split_payload)",
    number=10000,
    globals=globals(),
)
print(glob_perf)

glob_perf = timeit(
    stmt="count_pubs_global(split_payload)",
    number=10000,
    globals=globals(),
)
print(glob_perf)


local_perf = timeit(
    stmt="count_pubs(split_payload)",
    number=10000,
    globals=globals(),
)
print(local_perf)

local_perf = timeit(
    stmt="count_pubs(split_payload)",
    number=10000,
    globals=globals(),
)
print(local_perf)
