from time import perf_counter

bytes_list: list[bytes] = []

size = 1024 << 4
print(size)

for _ in range(size):
    bytes_list.append(b"asdf")


def make_bytes_regular(b_list: list[bytes]) -> bytes:
    res = b""

    for b in b_list:
        res += b

    return res


def make_bytes_join(b_list: list[bytes]) -> bytes:
    res = b""

    res.join(b_list)

    return res


s = perf_counter()

for _ in range(1000):
    make_bytes_regular(bytes_list)

e = perf_counter()
perf_regular = e - s
print(perf_regular)

s = perf_counter()

for _ in range(1000):
    make_bytes_join(bytes_list)

e = perf_counter()
perf_join = e - s
print(perf_join)

print(f"regular is {perf_regular/perf_join} times faster")
