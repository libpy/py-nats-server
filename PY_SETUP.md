
# Basic py setup
https://code.visualstudio.com/docs/python/python-tutorial

# Ruff - linter + formater
https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff

# mypy (Static types check)
https://mypy.readthedocs.io/en/stable/getting_started.html
https://marketplace.visualstudio.com/items?itemName=matangover.mypy

Add to json config (so it uses .venv)
"mypy.runUsingActiveInterpreter": true
https://docs.pydantic.dev/latest/integrations/visual_studio_code/

# pytest
https://docs.pytest.org/en/7.4.x/getting-started.html#get-started