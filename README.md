# py-nats-server

Copy of [NATS](https://nats.io/) server in Python. 

Trying to achieve comparable performance to real Nats while maintaining good Software Engineering practices and learning Python.

**Perf status: <del>~10</del> <del>~6 times slower</del> 40% of original NATS speed in 1:1 pub/sub 1M msgs** (1 Publisher sending 10M messages with 16B payload, and 1 Subscriber receiving all of them)

![perf_progress](perf_progress.png)

1) **Naive Py-Nats** - 3.13MB/sec\
This was just the simplest implementation of NATS messaging protocol that works.
2) **Buffer Output** - 10.76MB/sec\
Easy optimization, lets buffer up to 10 messages before sending to Subscriber.
3) **Read 8KiB batches socket** - 15.01MB/sec\
Similar idea as for 2), but this time for reading. The official [NatsCli](https://github.com/nats-io/natscli) used for performance testing is sending payloads in bulk. (Leter found out it was 256KiB)
4) **On Read empty out buffer** - 20.29MB/sec\
Complex component that buffered the Subscriber output [BufferWriter](https://gitlab.com/libpy/py-nats-server/-/blob/54a76c17ee16abaf7292dadb967c436600203eb1/src/client.py#L23) was simply deleted - it was overbuilt and too robust. In the main Server code when current Batch of data (up to 256KiB) is processed empty Buffer of each Subscriber.
5) **MSG info in Sub** - 22MB/sec\
I had one ugly piece of code that constructed MSG INFO inside Messenger.publish_msg, but after a code cleanup a new Subscription class was introduced and here a "base part" of MSG INFO is being precalculated.
6) **Upfront cmd split** - 25MB/sec\
Bytes are immutable, so instead of passing bytes around as function parameters, perform a split (on Space or new Line) and then pass list of bytes around (functions were splitting bytes parameters as first instruction).
7) **PUB take next as payload** - 52.45MB/sec\
Doubled the previous performance, HELL YEA :) When processing commands in batches (256KiB at the time), the next command is already there. This is useful only for [PUB](https://docs.nats.io/reference/reference-protocols/nats-protocol#pub), as after PUB the next command is PAYLOAD itself. So it was just as simple as taking the payload right away, lowering number of commands by a factor of 2 and getting the 2x speedup.
8) **Original_NATS** - 129.57MB/sec\
Original NATS server on my machine Ryzen 7900 with 12 cores. 

It's hard to beat original NATS speed.
TBD - remove asyncio, small python improvements (local vs global, map vs for vs []), multiprocessing with memory sharing, command parser written [directly in C](https://stackoverflow.com/a/40960403) (instead of using .split and .splitlines)

Details on performance progression with commits can be found at [perf_benchmark_readme.md](/perf_measurements/README.md)

## Goals:
1) Learn Python basics (setup dev environment, how to test, setup server, server side push etc..)
2) Write a drop-in replacement for NATS server, write black-box tests using Python Nats client [nats.py](https://github.com/nats-io/nats.py)

3) Focus on basic NATS functions:
- [CONNECT](https://docs.nats.io/reference/reference-protocols/nats-protocol#connect)
- [PING/PONG](https://docs.nats.io/reference/reference-protocols/nats-protocol#ping-pong)
- [SUB](https://docs.nats.io/reference/reference-protocols/nats-protocol#sub)
- [PUB](https://docs.nats.io/reference/reference-protocols/nats-protocol#pub)
- [UNSUB](https://docs.nats.io/reference/reference-protocols/nats-protocol#unsub)
4) Measure performance vs original NATS (written in Golang) - use [NATSCLI](https://github.com/nats-io/natscli)
5) Use NATSCLI to measure performance progression with each refactor
7) Organize everything really well, write tests and make it so you are proud of the code, module organization, test coverage and performance
6) Learn more on Python inner workings (threading, async, GC, memoryview, etc) to try and improve performance
7) Add more features, like [Queue Groups](https://docs.nats.io/nats-concepts/core-nats/queue)

## Experiments

### Sockets:
After experimenting a bit with Sockets in Python - found 2 ways to maintain multiple sockets:
1) [EPOLL raw socket](./experiments/0_epoll_raw_socket.py)
2) [Asyncio socket](./experiments/1_asyncio_socket.py)

The EPOLL one uses [Select OS primite](https://man7.org/linux/man-pages/man7/epoll.7.html) via [Selectors](https://docs.python.org/3/library/selectors.html) to maintain multiple socket.
There are some nuances with that solution:
1) Everything is still single threader so there will be a need to introduce Asyncio or Threads anyway - I changed my mind about this, it's not really needed!
2) Using raw socket it tricky, [READ](https://docs.python.org/3/library/socket.html#socket.socket.recv) from Socket can read only partially, or up to buff size. [SEND](https://docs.python.org/3/library/socket.html#socket.socket.send) can send only partially (it returns how much it sends)

Asyncion offers a lot nicer API with [Streams](https://docs.python.org/3/library/asyncio-stream.html) Reader & Writer. Additionally, Selectors is not needed so it's simpler and less code.

### Open questions on Sockets:

1) Is asyncio using EPOLL primite as best and the fastest way to get signal from Socket when it's ready or it's based on event loop?
(IT USES EPOLL!)
2) How big is coroutines overhead?