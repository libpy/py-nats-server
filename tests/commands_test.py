import pytest

import src.commands as commands
import src.client as client_m
import src.messenger as messenger

OK = b"+OK\r\n"
ERROR = b"-ERROR\r\n"

# needs pip install pytest-mock


def perform_cmd(mocker, test_input, expected, spy_func, spy_expected):
    client = client_m.Client(0, "ip", 0, None)
    nats_massanger = messenger.Messenger()

    if len(spy_func) != 0:
        spy = mocker.spy(nats_massanger, spy_func)

    runner = commands.CmdsRunner()
    result = runner._execute_next(client, nats_massanger, test_input.split())
    assert result == expected

    if len(spy_func) != 0:
        spy.assert_called_once_with(client, *spy_expected)


@pytest.mark.parametrize(
    "test_input, expected, spy_func, spy_expected",
    [
        (b"PUB t 3\r\n", b"", "", []),
        (b"SUB t 1\r\n", OK, "sub", [b"t", b"1"]),
        (b"UNSUB 1\r\n", ERROR, "unsub", [b"1"]),
        (b'CONNECT {"verbose":true}\r\n', OK, "connect", [b'{"verbose":true}']),
        (b'CONNECT {"verbose":false}\r\n', b"", "connect", [b'{"verbose":false}']),
        (b"PING\r\n", b"PONG\r\n", "", []),
    ],
)
def test_valid_cmds(mocker, test_input, expected, spy_func, spy_expected):
    perform_cmd(mocker, test_input, expected, spy_func, spy_expected)


@pytest.mark.parametrize(
    "test_input, expected, spy_func, spy_expected",
    [
        (b"PUB  t  3 \r\n", b"", "", []),
        (b"SUB  t  1 \r\n", OK, "sub", [b"t", b"1"]),
        (b"UNSUB  1 \r\n", ERROR, "unsub", [b"1"]),
        (b'CONNECT  {"verbose":true} \r\n', OK, "connect", [b'{"verbose":true}']),
        (b'CONNECT  {"verbose":false} \r\n', b"", "connect", [b'{"verbose":false}']),
        (b"PING \r\n", b"PONG\r\n", "", []),
    ],
)
def test_extra_space_valid_cmds(mocker, test_input, expected, spy_func, spy_expected):
    perform_cmd(mocker, test_input, expected, spy_func, spy_expected)


@pytest.mark.parametrize(
    "test_input, expected, spy_func, spy_expected",
    [
        (b"PAB t 3\r\n", ERROR, "", []),
        (b"UXSUB 1\r\n", ERROR, "", []),
        (b'XXXX {"verbose":true}\r\n', ERROR, "", []),
        (b'YYY {"verbose":false}\r\n', ERROR, "", []),
        (b"ASDFG\r\n", ERROR, "", []),
    ],
)
def test_invalid_cmds(mocker, test_input, expected, spy_func, spy_expected):
    perform_cmd(mocker, test_input, expected, spy_func, spy_expected)
