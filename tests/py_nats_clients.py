import asyncio
import nats
import nats.errors

import multiprocessing
import time
import logging

# TODO looks like a hacky way to import things
import sys

sys.path.append("src")
import server


def run_py_nats_server():
    asyncio.run(server.start_server())


def publish_msgs(topic: str, payload: str, msgCnt: int):
    async def publish():
        nc = await nats.connect("nats://localhost:4222", allow_reconnect=False)

        if msgCnt != -1:
            for i in range(msgCnt):
                await nc.publish(topic, f"{payload}_{i}".encode())
        else:
            while True:
                await nc.publish(topic, payload.encode())
                await asyncio.sleep(0.001)

        await nc.close()

    asyncio.run(publish())


async def test_30_1_fanout():
    py_nats_server_process = multiprocessing.Process(target=run_py_nats_server)
    py_nats_server_process.start()
    time.sleep(0.1)  # enough time for server to start on different process

    topic = "foo"
    nc = await nats.connect("nats://localhost:4222", allow_reconnect=False)
    sub = await nc.subscribe(topic)
    try:
        await sub.next_msg(
            0.0001
        )  # it's lazy, wont subscribe in reality without this...
    except:
        pass

    payload = "bar"
    msgCnt = 1000
    pubCnt = 30

    msgsToReceive: list[bytes] = []
    for _ in range(pubCnt):
        for i in range(msgCnt):
            msgsToReceive.append(f"{payload}_{i}".encode())

    pubs: list[multiprocessing.Process] = []
    for _ in range(pubCnt):
        p = multiprocessing.Process(
            target=publish_msgs,
            args=(
                topic,
                payload,
                msgCnt,
            ),
        )
        p.start()
        pubs.append(p)

    for _ in range(msgCnt * pubCnt):
        try:
            msg = await sub.next_msg(1)
            assert msg.subject == topic

            if msg.data not in msgsToReceive:
                raise AssertionError(f"unknown msg: {msg.data.decode()}")
            msgsToReceive.remove(msg.data)
        except nats.errors.TimeoutError:
            raise AssertionError("next_msg failed on timeout")

    for p in pubs:
        p.join()

    py_nats_server_process.kill()
    print("Fanout passed!")
    await nc.close()


def wait_for_msgs(id: int, topic: str, payload: str, msgCnt: int):
    async def subscribe():
        nc = await nats.connect("nats://localhost:4222", allow_reconnect=False)
        sub = await nc.subscribe(topic)

        msgsToReceive: list[bytes] = []
        for i in range(msgCnt):
            msgsToReceive.append(f"{payload}_{i}".encode())

        try:
            first_msg = await sub.next_msg(1)
            if first_msg.data not in msgsToReceive:
                raise AssertionError(f"{id}: unknown msg: {first_msg.data.decode()}")
            msgsToReceive.remove(first_msg.data)
        except nats.errors.TimeoutError:
            raise AssertionError(f"{id}: no msg after 1 second")

        prev_msg = b""
        for _ in range(msgCnt - 1):
            try:
                msg = await sub.next_msg(5)
                assert msg.subject == topic

                if msg.data not in msgsToReceive:
                    raise AssertionError(f"{id}: unknown msg: {msg.data.decode()}")
                msgsToReceive.remove(msg.data)
                prev_msg = msg.data
            except nats.errors.TimeoutError:
                raise AssertionError(
                    f"{id}: prev({prev_msg.decode()})next_msg failed on timeout"
                )

        assert len(msgsToReceive) == 0

        await nc.close()

    asyncio.run(subscribe())


async def test_1_30_fanin():
    py_nats_server_process = multiprocessing.Process(target=run_py_nats_server)
    py_nats_server_process.start()
    time.sleep(0.1)  # enough time for server to start on different process
    topic = "foo"
    payload = "bar"
    msgCnt = 1000
    subCnt = 30

    subs: list[multiprocessing.Process] = []
    for i in range(subCnt):
        p = multiprocessing.Process(
            target=wait_for_msgs,
            args=(
                i,
                topic,
                payload,
                msgCnt,
            ),
        )
        p.start()
        subs.append(p)

    await asyncio.sleep(0.1)
    nc = await nats.connect(
        "nats://localhost:4222",
        allow_reconnect=False,
    )
    for i in range(msgCnt):
        await nc.publish(topic, f"{payload}_{i}".encode())

    await nc.drain()
    for p in subs:
        p.join()

    py_nats_server_process.kill()
    print("Fanin passed!")
    await nc.close()


async def publish_always_unsub_after_msgs():
    py_nats_server_process = multiprocessing.Process(target=run_py_nats_server)
    py_nats_server_process.start()
    time.sleep(0.1)  # enough time for server to start on different process
    topic = "foo"
    payload = "bar"
    msgCnt = 100

    nc = await nats.connect("nats://localhost:4222", allow_reconnect=False)
    sub = await nc.subscribe(topic)
    try:
        await sub.next_msg(
            0.0001
        )  # it's lazy, wont subscribe in reality without this...
    except:
        pass

    p = multiprocessing.Process(
        target=publish_msgs,
        args=(
            topic,
            payload,
            -1,
        ),
    )
    p.start()

    for _ in range(msgCnt):
        msg = await sub.next_msg(0.1)
        assert msg.data == payload.encode()
        assert msg.subject == topic

    await sub.unsubscribe()

    try:
        await sub.next_msg(0.1)
        raise AssertionError("unsubscribed happened shoudn't recive more msgs")
    except:
        pass

    p.kill()
    py_nats_server_process.kill()
    print("Unsub passed!")
    await nc.close()


if __name__ == "__main__":
    logging.basicConfig(level="ERROR")

    async def run_tests():
        await publish_always_unsub_after_msgs()
        await test_30_1_fanout()
        await test_1_30_fanin()

    asyncio.run(run_tests())
