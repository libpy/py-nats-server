from src.messenger import Messenger, OK, SubKey
from src.client import Client
from src.commands import CmdsRunner

IP = "127.0.0.1"


def test_pub_sub_details():
    pub_port = 1
    pub_client = Client(0, IP, pub_port, None)

    sub_port = 2
    sub_client = Client(1, IP, sub_port, None)

    nats = Messenger()

    # Subscribe to topic "test"
    runner = CmdsRunner()
    sub_res = runner._execute_next(sub_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK
    assert len(nats.subscriptions) == 1
    topic = b"test"
    assert topic in nats.subscriptions

    assert len(nats.sub_lookup) == 1
    key = SubKey(sub_client, b"sid_1")
    assert key in nats.sub_lookup

    subs = nats.subscriptions[topic]
    assert len(subs) == 1

    sub = subs[0]
    assert sub_client.id == sub.consumer.id
    assert b"sid_1" == sub.sid

    # Announce what will you publish to topic "test"
    runner._execute_next(pub_client, nats, b"PUB test 3\r\n".split())
    assert any(runner.prev_cmd_pub_parts)

    pub_topic, b_bytes_cnt = runner.prev_cmd_pub_parts

    assert pub_topic == b"test"
    assert int(b_bytes_cnt) == 3

    # Perform the publish it self
    msg = b"ABC"
    pub_res, ok = nats.publish_msg(pub_client, pub_topic, b_bytes_cnt, msg)
    assert ok

    assert pub_res == OK

    assert len(nats.clients_to_drain) == 1
    drain_client = nats.clients_to_drain[0]
    assert drain_client.id == sub_client.id
    buf = nats.clients_to_drain[0].buffer
    assert len(buf) == 5  # msg_info + msg it self

    assert buf[0] == b"MSG test sid_1 "
    assert buf[1] == b"3"
    assert buf[2] == b"\r\n"
    assert buf[3] == msg
    assert buf[4] == b"\r\n"


def new_client(id: int):
    return Client(id, IP, id, None)


def sub_4(nats: Messenger):
    sub1_client = new_client(1)
    sub2_client = new_client(2)
    sub3_client = new_client(3)

    runner = CmdsRunner()
    sub_res = runner._execute_next(sub1_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK
    sub_res = runner._execute_next(sub1_client, nats, b"SUB test sid_2\r\n".split())
    assert sub_res == OK
    sub_res = runner._execute_next(sub2_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK
    sub_res = runner._execute_next(sub3_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK

    return sub1_client, sub2_client, sub3_client


def test_multi_sub_disconnect():
    nats = Messenger()

    c1, c2, c3 = sub_4(nats)

    assert len(nats.subscriptions) == 1
    assert len(nats.subscriptions[b"test"]) == 4

    cur_len = len(nats.sub_lookup)
    nats.disconnect(c1)
    assert cur_len == len(nats.sub_lookup) + 2, "first one has 2x sub"
    cur_len = len(nats.sub_lookup)
    nats.disconnect(c2)
    assert cur_len == len(nats.sub_lookup) + 1, "second one has 1x sub"
    cur_len = len(nats.sub_lookup)
    nats.disconnect(c3)
    assert cur_len == len(nats.sub_lookup) + 1, "second one has 1x sub"

    assert len(nats.subscriptions) == 0


def test_multi_sub_unsub():
    nats = Messenger()

    c1, c2, c3 = sub_4(nats)

    assert len(nats.subscriptions) == 1
    assert len(nats.subscriptions[b"test"]) == 4

    nats.unsub(c1, b"sid_1")
    assert len(nats.subscriptions[b"test"]) == 4, "No change just marked delete"

    nats.unsub(c1, b"sid_2")
    assert len(nats.subscriptions[b"test"]) == 4, "No change just marked delete"

    nats.unsub(c2, b"sid_1")
    assert len(nats.subscriptions[b"test"]) == 1, "Should drop now with 3rd unsub"

    nats.unsub(c3, b"sid_1")
    assert (
        b"test" not in nats.subscriptions
    ), "When 1 is previos 1 > 0 so drop completly"

    assert len(nats.subscriptions) == 0


def test_fanout_1_3():
    pub_client = new_client(0)

    sub1_client = new_client(1)
    sub2_client = new_client(2)
    sub3_client = new_client(3)

    nats = Messenger()

    runner = CmdsRunner()
    sub_res = runner._execute_next(sub1_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK
    sub_res = runner._execute_next(sub2_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK
    sub_res = runner._execute_next(sub3_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK

    msg = b"ABC"

    # Publish to 1test1, no one is listening to it!
    pub_res, _ = nats.publish_msg(pub_client, b"test1", b"3", msg)
    assert pub_res == OK

    # Publish to test
    pub_res, _ = nats.publish_msg(pub_client, b"test", b"3", msg)
    assert pub_res == OK

    assert len(nats.clients_to_drain) == 3
    drain_client_1 = nats.clients_to_drain[0]
    drain_client_2 = nats.clients_to_drain[1]
    drain_client_3 = nats.clients_to_drain[2]

    assert drain_client_1.id == sub1_client.id
    assert drain_client_2.id == sub2_client.id
    assert drain_client_3.id == sub3_client.id

    msg_info = b"MSG test sid_1 3\r\n"
    full_msg = msg_info + msg + b"\r\n"

    for c in nats.clients_to_drain:
        assert len(c.buffer) == 5, "msg_info_base + b_cnt + crlf + msg + crlf"
        assert b"".join(c.buffer) == full_msg


def test_fanin_3_1():
    sub_client = new_client(0)

    pub1_client = new_client(1)
    pub2_client = new_client(2)
    pub3_client = new_client(3)

    nats = Messenger()

    runner = CmdsRunner()
    sub_res = runner._execute_next(sub_client, nats, b"SUB test sid_1\r\n".split())
    assert sub_res == OK

    msg = b"ABC"

    # Publish to test 3x (from pub1, pub2 and pub3)
    pub_res, _ = nats.publish_msg(pub1_client, b"test", b"3", msg)
    assert pub_res == OK
    assert len(sub_client.buffer) == 5

    pub_res, _ = nats.publish_msg(pub2_client, b"test", b"3", msg)
    assert pub_res == OK
    assert len(sub_client.buffer) == 10

    pub_res, _ = nats.publish_msg(pub3_client, b"test", b"3", msg)
    assert pub_res == OK
    assert len(sub_client.buffer) == 15

    msg_info = b"MSG test sid_1 3\r\n"

    assert len(nats.clients_to_drain) == 1
    drain_client_1 = nats.clients_to_drain[0]
    assert drain_client_1.id == sub_client.id

    assert len(drain_client_1.buffer) == 15

    full_msgs = (
        msg_info + msg + b"\r\n" + msg_info + msg + b"\r\n" + msg_info + msg + b"\r\n"
    )
    buff_msgs = b"".join(drain_client_1.buffer)

    assert full_msgs == buff_msgs
