import src.splitter as splitter
import pytest


@pytest.mark.parametrize(
    "test_input, expected",
    [
        (b"", []),
        (b"\r\n", [b""]),
        (b"\r", [b""]),
        (b"\n", [b""]),
        (b"a\r\nb\r\n", [b"a", b"b"]),
    ],
)
def test_single_split(test_input, expected):
    s = splitter.CommandsSplitter()

    res = s.carry_over_split(test_input.splitlines(), ord(b"\n"))

    assert res == expected


@pytest.mark.parametrize(
    "test_inputs, expected",
    [
        ([b"a\r\nb\r\n", b"c\r\nd\r\n"], [b"a", b"b", b"c", b"d"]),
        (
            [b"aba\r\nba", b"b\r\nd\r\n"],
            [
                b"aba",
                b"bab",
                b"d",
            ],
        ),
        (
            [b"dfg\r\ndcf", b"\r\na\r\n"],
            [
                b"dfg",
                b"dcf",
                b"a",
            ],
        ),
        (
            [b"xcv\r\nxsw\r", b"\nf\r\n"],
            [
                b"xcv",
                b"xsw",
                b"f",
            ],
        ),
        (
            [b"q\r\nw", b"e\r\nr", b"\r\nt\r\n", b"y\r\nu", b"i\r\no\r\n"],
            [
                b"q",
                b"we",
                b"r",
                b"t",
                b"y",
                b"ui",
                b"o",
            ],
        ),
    ],
)
def test_more_splits(test_inputs, expected):
    s = splitter.CommandsSplitter()

    res = []
    for payload in test_inputs:
        res.extend(s.carry_over_split(payload.splitlines(), payload[-1]))

    assert res == expected
